package innovationportal;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.sql.*;
import java.util.ArrayList;
import java.util.Map;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

import org.apache.commons.codec.binary.Base64;

public class Database {

	/*
	 * If you are trying to connect from your local computer, connect to the 411 VM
	 * run the command sudo mysql then run 
	 * GRANT ALL PRIVILEGES ON *.* TO 'root'@'PUBIP.PUBIP.PUBIP.%' IDENTIFIED BY 'aMaZinG^#3' WITH GRANT OPTION;
	 * where PUBIP is your public IP and % is a wild card.
	 * 
	 * Before pushing to git make sure you are using the local string
	 */

	// https://mariadb.com/kb/en/library/java-connector-using-gradle/

	// private final String local = "localhost";
	// private final String remote = "128.82.11.82";
	// private final String database = "IP";
	private final String pass = "aMaZinG^#3";
	private static Connection conn = null;

	Database() throws ClassNotFoundException, SQLException{
		Class.forName("org.mariadb.jdbc.Driver");
		try {
			conn = DriverManager.getConnection("jdbc:mariadb://localhost:3306/IP", "root", pass);
		}catch (SQLException e1) {
			conn = DriverManager.getConnection("jdbc:mariadb://128.82.11.82:3306/IP", "root", pass);
		}
	}
	
	Database(Connection _conn){
		conn = _conn;
	}

	/**
	 * populates featured cache with given project IDs
	 * @param cache
	 * @param projectIDs
	 * @throws SQLException 
	 */
	public void populateFeaturedCache(Cache<Integer, Projects> cache, int[] projectIDs) throws SQLException {
		for(Integer id : projectIDs) {
			cache.put(id, this.getProject(id));
		}
		
	}

	/**
	 * populates cache with randomly selected projects
	 * @param cache
	 * @param _limit
	 * @throws SQLException
	 */
	public void populateCache(Cache<Integer, Projects> cache, int _limit,Cache<Integer, Projects> featured) throws SQLException {

		 String query = 
				"SELECT projectID, P1.displayName,description " +
				"FROM Project P1 " +
				"ORDER BY RAND()" +
				"LIMIT ?";

		PreparedStatement stmt = conn.prepareStatement(query);
		stmt.setInt(1, _limit);
		ResultSet result = stmt.executeQuery();

		while (result.next()) {
			int projectID = result.getInt(1);
			if(!featured.exists(projectID)) {
				if(!cache.exists(projectID)) {
					String displayName = result.getString(2);
					String description = result.getString(3);
					
					//gets the project thumbnail
					String artifactLoc = null;
					Artifacts thumbnail = this.getProjectThumbnail(projectID);
					if(thumbnail != null) {
						artifactLoc = thumbnail.getArtifactLocation();
					}
					
					Projects project = new Projects(projectID, displayName, description, artifactLoc);
					cache.put(result.getInt(1), project);
				}
			}
		}
		result.close();
		stmt.close();
	}

	/**
	 * gets all project information for a given ID
	 * @param projectID
	 * @return Projects
	 * @throws SQLException
	 */
	@SuppressWarnings("unused")
	public Projects getProject(int projectID) throws SQLException {
		String query = 
				"SELECT P1.projectID, P1.displayName, P1.description " + 
				"FROM Project P1 " +
				"WHERE P1.projectID = ?";
		
		PreparedStatement stmt = conn.prepareStatement(query);
		stmt.setInt(1, projectID);
		ResultSet result = stmt.executeQuery();

		ArrayList<Member> members = new ArrayList<Member>();
		ArrayList<Artifacts> artifacts = new ArrayList<Artifacts>();
		ArrayList<Collaboration> collabs = new ArrayList<Collaboration>();
		ArrayList<String> tags = new ArrayList<String>();
		String thumbnailLoc = null;
		
		result.next();
		String displayName = result.getString(2); //cant be null
		if(result.wasNull()) { return null;}
		String description = result.getString(3); //cant be null
		if(result.wasNull()) { return null;}

		
		Projects project = new Projects(projectID, displayName, description,
				this.getProjectThumbnail(projectID).getArtifactLocation(),
				this.getProjectArtifacts(projectID),
				this.getProjectCollab(projectID),
				this.getProjectTags(projectID),
				this.getProjectMembers(projectID));
		result.close();
		stmt.close();
		return project;
	}

	/**
	 * returns the permissions the user has over the entire site
	 * @param userEmail
	 * @return String permission
	 * @throws SQLException
	 */
	public String getUserPermissions(String userEmail) throws SQLException {
		String permission = null;
		String query = 
				"SELECT permission " +
				"FROM User " +
				"WHERE email = ?";

		PreparedStatement stmt = conn.prepareStatement(query);
		stmt.setString(1, userEmail);
		ResultSet result = stmt.executeQuery();

		// checks if the result set is empty
		if (!result.next()) {
			System.out.println("Error: Empty result set");
		} else {
			permission = result.getString(1);
			if(result.wasNull()) { return null; }
		}
		result.close();
		stmt.close();
		return permission;
	}

	/**
	 * gets the role the user has over a project
	 * @param projectID
	 * @param userEmail
	 * @return String role
	 * @throws SQLException
	 */
	public String getUserRole(int projectID, String userEmail) throws SQLException {
		String userRole = "";
		String query = 
				"SELECT PM1.roleName " + 
				"FROM ProjectMember PM1 " + 
				"WHERE (PM1.userID = ? " +
				"AND PM1.projectID = ?) ;";
		PreparedStatement stmt = conn.prepareStatement(query);
		stmt.setString(1, userEmail);
		stmt.setInt(2, projectID);
		ResultSet result = stmt.executeQuery();
		// checks if the result set is empty
		if (!result.next()) {
			System.out.println("Error: Empty result set");
		} else {
			userRole = result.getString(1);
			if(result.wasNull()) { return null; }
		}
		result.close();
		stmt.close();
		return userRole;
	}

	/**
	 * gets creator of a projects name
	 * @param projectID
	 * @return String firstname + lastname
	 * @throws SQLException
	 */
	public String getProjectCreator(int projectID) throws SQLException {
		
		String creator = "";
		String query = 
				"SELECT U1.firstName, U1.lastName " +
				"FROM User U1 " +
					"WHERE EXISTS (SELECT userID " +
					"FROM ProjectMember PM1 " +
					"WHERE PM1.userID = U1.email " +
					"AND PM1.roleName = 'Creator' " +
					"AND projectID = ?) ;";
		
		PreparedStatement stmt = conn.prepareStatement(query);
		stmt.setInt(1, projectID);
		ResultSet result = stmt.executeQuery();

		// checks if the result set is empty
		if (!result.next()) {
			System.out.println("Error: Empty result set");
		} else {
			creator = result.getString(1) + " " + result.getString(2);
		}
		result.close();
		stmt.close();
		return creator;
	}

	/**
	 * gets the email of the project creator
	 * @param projectID
	 * @return String email
	 * @throws SQLException
	 */
	public String getProjectCreatorEmail(int projectID) throws SQLException {
		String creatorEmail = null;
		String query = 
				"SELECT PM1.userID " +
				"FROM ProjectMember PM1 " +
					"WHERE (PM1.projectID = ? " +
					"AND PM1.roleName = 'Creator');";

		PreparedStatement stmt = conn.prepareStatement(query);
		stmt.setInt(1, projectID);
		ResultSet result = stmt.executeQuery();

		// checks if the result set is empty
		if (!result.next()) {
			System.out.println("Error: Empty result set");
		} else {
			creatorEmail = result.getString(1);
		}
		result.close();
		stmt.close();
		return creatorEmail;
	}
	
	/**
	 * gets all the projects that contain the searched term
	 * @param term
	 * @return ArrayList<Project>
	 * @throws SQLException
	 */
	public ArrayList<Projects> searchFunction(String term) throws SQLException {
		//TODO search project title and project tags for the term,
		//any projects that contain it should be returned
		
		ArrayList<Projects> projects = new ArrayList<Projects>();
		String query = 
				"SELECT projectID, P1.displayName,description " +
				"FROM Project P1 " +
				"LEFT JOIN TagInformation T1 " +
				"ON P1.projectID = T1.PID " +
				"WHERE P1.displayName like ? " +
				"OR T1.tagName like ? "+ 
				"GROUP BY projectID, P1.displayName,description";
		PreparedStatement stmt = conn.prepareStatement(query);
		String searchValue = "%" + term + "%";
		stmt.setString(1, searchValue);
		stmt.setString(2, searchValue);
		ResultSet result = stmt.executeQuery();
		while(result.next()) {
			int id = result.getInt(1);
			String displayName = result.getString(2);
			String desc = result.getString(3);
			
			String thumbnailLoc = null;
			Artifacts thumbnail = this.getProjectThumbnail(id);
			if(thumbnail != null) {
				thumbnailLoc = thumbnail.getArtifactLocation();
			}
			
			Projects tempProject = new Projects(id,displayName,desc,thumbnailLoc);
			if(!projects.contains(tempProject)) {
				projects.add(tempProject);
			}
		}
		result.close();
		stmt.close();
		return projects;
	}
	
	/**
	 * inserts the artifact data into the database
	 * @param projectID
	 * @param location
	 * @param type
	 * @return true for success
	 * @throws SQLException
	 */
	public boolean uploadArtifact(int projectID,String location,String type) throws SQLException {
		//insert artifact row using project ID, location will be stored under name
		//type will be stored under displayName
		String artifactUpdate =
			"INSERT INTO Artifact " +
			"(projID,name,displayName)" +
			"VALUES(?,?,?);";
		PreparedStatement stmt = conn.prepareStatement(artifactUpdate);
		stmt.setInt(1,projectID);
		stmt.setString(2, location);
		stmt.setString(3, type);
		int success = stmt.executeUpdate();
		stmt.close();
		//Check if executeUpdate() inserted at least 1 line.
		//If noticing issues, could change to query the database
		//to validate Artifact was inserted properly instead of just
		//checking that a line insertion was made.
		if (success > 0)
			return true;
		return false;
	}
	
	/**
	 * returns member info needed for UI, null for login failure
	 * @param userEmail
	 * @param password
	 * @return Member
	 * @throws InvalidKeySpecException
	 * @throws NoSuchAlgorithmException
	 * @throws SQLException
	 */
	public Member loginFunction(String userEmail,String password) throws InvalidKeySpecException, NoSuchAlgorithmException, SQLException {
		
		String query = 
		"SELECT passwordSalt, passwordHash, firstName, lastName, displayName, permission " +
		"FROM User U1 " +
			"WHERE U1.email = ?";
		//checkPasswordStmt is SQL statement to query User table for user details of given email.
		PreparedStatement checkPasswordStmt = conn.prepareStatement(query);
		checkPasswordStmt.setString(1, userEmail);
		ResultSet userQuery = checkPasswordStmt.executeQuery();
		//Check if the email was found in the database, if not return null.
		if (!userQuery.next()) {
			System.out.println("Error: Empty result set, no user found from Email");
			userQuery.close();
			checkPasswordStmt.close();
			return null;
		}
		//Else get salt and hash from the database
		else {
			byte[] salt = Base64.decodeBase64(userQuery.getString(1));
			KeySpec spec = new PBEKeySpec(password.toCharArray(), salt, 65536, 128);
			SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
			byte[] hash = factory.generateSecret(spec).getEncoded();
			byte[] passwdHash = Base64.decodeBase64(userQuery.getString(2));
			
			//Check password, if no match then return null
			if(passwdHash.length == hash.length){     
			   	for(int i=0 ;i<passwdHash.length; i++){
					if(passwdHash[i] != hash[i]){
						userQuery.close();
						checkPasswordStmt.close();
						return null;
					}
				}
			}
			//Pull the rest of user info from User table
			String firstName = userQuery.getString(3);
			String lastName = userQuery.getString(4);
			String displayName = userQuery.getString(5);
			String permission = userQuery.getString(6);
			userQuery.close();
			checkPasswordStmt.close();				    
			//use member's login constructor below to create a member and then return it
			//Member(String _memberName, String _memberDisplayName, String _email, String _permissionType,
				//	ArrayList<Integer> _creator,ArrayList<Integer> _moderator,ArrayList<Integer> _participant)
			String projectMemberQuery =
				"SELECT (GROUP_CONCAT(ProjectMember.projectID)) Projects, roleName " +
				"FROM ProjectMember " +
				"WHERE userID = ? " +
				"GROUP BY roleName; ";
			//pMemberStmt is the SQL statement to pull projects and roles of a given user
			PreparedStatement pMemberStmt = conn.prepareStatement(projectMemberQuery);
			pMemberStmt.setString(1, userEmail);
			ResultSet pMemberResult = pMemberStmt.executeQuery();

			
			
			ArrayList<Integer> creatorProjects = new ArrayList<Integer>();
			ArrayList<Integer> moderatorProjects = new ArrayList<Integer>();
			ArrayList<Integer> participantProjects = new ArrayList<Integer>();
			String projectList = null, projectRole;
			while (pMemberResult.next()){
				if(!pMemberResult.wasNull()) {
					projectList = pMemberResult.getString(1); //list of project ids
					projectRole = pMemberResult.getString(2); // the role for those project
					if(projectList != null) {
						if(projectList.contains(",")) {
							String[] projectListString = projectList.split(",");
							for (int i = 0; i < projectListString.length; i++){
								if (projectRole.contains(("Mentor"))){
									moderatorProjects.add(Integer.parseInt(projectListString[i]));
								}
								else if (projectRole.contains(("Creator"))){
									creatorProjects.add(Integer.parseInt(projectListString[i]));
								}
								else if (projectRole.contains(("Participant"))){
									participantProjects.add(Integer.parseInt(projectListString[i]));
								}
							}
						}
						else {
							if (projectRole.contains(("Mentor"))){
								moderatorProjects.add(Integer.parseInt(projectList));
							}
							else if (projectRole.contains(("Creator"))){
								creatorProjects.add(Integer.parseInt(projectList));
							}
							else if (projectRole.contains(("Participant"))){
								participantProjects.add(Integer.parseInt(projectList));
							}
						}
					}
				}
			}
			pMemberResult.close();
			pMemberStmt.close();
			Member returnMember = new Member(firstName + " " + lastName, displayName, userEmail, permission,
			creatorProjects, moderatorProjects, participantProjects);
			return returnMember;
		}
	}
	
	/**
	 * registers a new user into the database
	 * @param email
	 * @param password
	 * @param fName
	 * @param lName
	 * @param displayName
	 * @return true for success
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeySpecException
	 * @throws SQLException
	 */
	public boolean registerUser(String email, String password, String fName, String lName, String displayName) throws NoSuchAlgorithmException, InvalidKeySpecException, SQLException {	
		String emailQuery = 
			"SELECT email " +
			"FROM User " +
			"WHERE User.email = ?";
		//emailStmt is the SQL statement to query for email to see if it already exists
		PreparedStatement emailStmt = conn.prepareStatement(emailQuery);
		emailStmt.setString(1, email);
		ResultSet emailResult = emailStmt.executeQuery();
		//If email already exists, return false.
		if (emailResult.next()) {
			System.out.println("Error: Empty result set, email already exists");
			emailResult.close();
			emailStmt.close();
			return false;
		} 
		//Otherwise generate salt and hash
		else{
			byte[] salt = new byte[64];
			SecureRandom random = new SecureRandom();
			random.nextBytes(salt);
			KeySpec spec = new PBEKeySpec(password.toCharArray(), salt, 65536, 128);
			SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
			byte[] hash = factory.generateSecret(spec).getEncoded();
			//Insert new User with given parameters, default permission is 'User'
			String registerUpdate =
			"INSERT INTO User " +
			"(email, firstName, lastName, displayName, passwordSalt, passwordHash, permission) " +
			"VALUES(?,?,?,?,?,?,'user');";
			//registerStmt is the SQL statement to insert new user into User table
			PreparedStatement registerStmt = conn.prepareStatement(registerUpdate);
			registerStmt.setString(1,email);
			registerStmt.setString(2,fName);
			registerStmt.setString(3,lName);
			registerStmt.setString(4,displayName);
			registerStmt.setString(5,Base64.encodeBase64String(salt));
			registerStmt.setString(6,Base64.encodeBase64String(hash));
			int success = registerStmt.executeUpdate();
			emailResult.close();
			emailStmt.close();
			registerStmt.close();
			//checks if a row was affected
			if (success > 0)
				return true;
		}
		//Default case
		return false;
	}
	
	/**
	 * updates the project views
	 * @param visitedCount
	 * @throws SQLException 
	 */
	public void updateViews(Map<Integer,Integer> visitedCount) throws SQLException {
		String query = 
				"UPDATE Project " +
				"SET pageHitCount = pageHitCount + ? " +
				"WHERE projectID = ? ";
		for(Map.Entry<Integer, Integer> project : visitedCount.entrySet()) {
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setInt(1,project.getValue());
			stmt.setInt(2, project.getKey());
			int success = stmt.executeUpdate();
			stmt.close();
			if(success >0) {
				visitedCount.remove(project.getKey());
			}
		}
	}
	
	public int createProject(String creator,String projectName, String projectDescription) throws SQLException {
		String query = 
				 "INSERT INTO Project "//1
				   + "(displayName,description,permission) "
				   + "VALUES( ? , ? ,' ')";
		PreparedStatement stmt = conn.prepareStatement(query);
		stmt.setString(1,projectName);
		stmt.setString(2, projectDescription);
		int success = stmt.executeUpdate();
		stmt.close();
		if(success>0) {
			query = "SELECT LAST_INSERT_ID()";
			stmt = conn.prepareStatement(query);
			ResultSet rs = stmt.executeQuery();
			if(rs.next()) {
				
				int id = rs.getInt(1);
				rs.close();
				stmt.close();
				
				query = 
						 "INSERT INTO ProjectMember "//1
						   + "(projectID,userID,roleName) "
						   + "VALUES( ? , ? , ? )";
				stmt = conn.prepareStatement(query);
				stmt.setInt(1,id);
				stmt.setString(2, creator);
				stmt.setString(3, "Creator");
				success = stmt.executeUpdate();
				if(success >0) {
					return id;
				}
			}
		}
		return -1;
	}
	
	public ArrayList<Projects> getAllProjects() throws SQLException {
		ArrayList<Projects> projects = new ArrayList<Projects>();
		
		String query = 
				 "SELECT projectID, displayName "//1
				   + "FROM Project ";
		PreparedStatement stmt = conn.prepareStatement(query);
		ResultSet rs = stmt.executeQuery();
		while(rs.next()) {
			int id = rs.getInt(1);
			String displayName = rs.getString(2);
			projects.add(new Projects(id,displayName,this.getProjectMembers(id)));
		}
		
		return projects;
	}

	/**
	 * gets an array list of all projects without a mentor
	 * @return ArrayList<Projects>
	 * @throws SQLException
	 */
	public ArrayList<Projects> getProjectsNeedingMod() throws SQLException {
		ArrayList<Projects> projects = new ArrayList<Projects>();
		ArrayList<Integer> ids = new ArrayList<Integer>();
		
		//adds all project ids to the list
		String query = 
				 "SELECT projectID "//1
				   + "FROM Project ";
		PreparedStatement stmt = conn.prepareStatement(query);
		ResultSet rs = stmt.executeQuery();
		while(rs.next()) {
			ids.add(rs.getInt(1));
		}
		rs.close();
		stmt.close();
		
		//removes projects with mentors from the list
		query = 
				 "SELECT projectID "//1
				   + "FROM ProjectMember "
				   + "WHERE roleName = ?";
		stmt = conn.prepareStatement(query);
		stmt.setString(1, "Mentor");
		rs = stmt.executeQuery();
		while(rs.next()) {
			ids.remove(Integer.valueOf(rs.getInt(1)));
		}
		
		for(Integer id : ids) {
			projects.add(this.getProject(id));
		}
		
		return projects;
	}
	
	public ArrayList<Projects> getProjectsByViews() throws SQLException {
		ArrayList<Projects> projects = new ArrayList<Projects>();
		
		//adds all project ids to the list
		String query = 
				 "SELECT projectID, pageHitCount "//1
				   + "FROM Project "
				   + "ORDER BY pageHitCount DESC "
				   + "LIMIT ?";
		PreparedStatement stmt = conn.prepareStatement(query);
		stmt.setInt(1, Main.MAX_PROJECTS);
		ResultSet rs = stmt.executeQuery();
		while(rs.next()) {
			Projects current = this.getProject(rs.getInt(1));
			current.setViews(rs.getInt(2));
			projects.add(current);
		}
		rs.close();
		stmt.close();
		
		return projects;
	}
	
	/**
	 * adds the newest projects to an arraylist
	 * @return ArrayList<projects>
	 * @throws SQLException
	 */
	public ArrayList<Projects> getProjectsByDate() throws SQLException {
		ArrayList<Projects> projects = new ArrayList<Projects>();
		
		//adds all project ids to the list
		String query = 
				 "SELECT projectID, dateCreated "//1
				   + "FROM Project "
				   + "ORDER BY dateCreated DESC "
				   + "LIMIT ? ";
		PreparedStatement stmt = conn.prepareStatement(query);
		stmt.setInt(1, Main.MAX_PROJECTS);
		ResultSet rs = stmt.executeQuery();
		while(rs.next()) {
			Projects current = this.getProject(rs.getInt(1));
			current.setDateCreated(rs.getDate(2));
			projects.add(current);
		}
		rs.close();
		stmt.close();
		
		return projects;
	}
	
	public int removeProjectArtifact(int projectID, String artifact) throws SQLException {
		String query = 
				 "DELETE FROM Artifact "//1
				   + "WHERE projID = ? "
				   + "AND displayName = ? ";
		PreparedStatement stmt = conn.prepareStatement(query);
		stmt.setInt(1,projectID);
		stmt.setString(2, artifact);
		int success = stmt.executeUpdate();
		stmt.close();
		
		return success;
	}
	
	public Member getUserInfo(String userEmail) throws SQLException {
		Member member = null;
		String query = "SELECT firstName, lastName, displayName " +
				"FROM User U1 " +
				"WHERE U1.email = ? ";
		
		PreparedStatement pMemberStmt = conn.prepareStatement(query);
		pMemberStmt.setString(1, userEmail);
		ResultSet result = pMemberStmt.executeQuery();
		if(result.next()) {
			String fName =result.getString(1);
			String lName =result.getString(2);
			String displayName =result.getString(3);
			member = new Member(fName + " " + lName, displayName, userEmail, null,
					null, null, null);
		}
		return member;
	}
	
	public boolean appendProjectMember(String userEmail, int projectID) throws SQLException {
		String query = 
				 "INSERT INTO ProjectMember "//1
				   + "(projectID,userID,roleName) "
				   + "VALUES( ? , ? , 'Participant' ) ";
		PreparedStatement stmt = conn.prepareStatement(query);
		stmt.setInt(1, projectID);
		stmt.setString(2, userEmail);
		int success = stmt.executeUpdate();
		stmt.close();
		
		if(success>0) {
			return true;
		}
		return false;
	}
	
	public boolean appendCollabInfo(int projectID, String gitURL, String slackURL) throws SQLException {
		String query = 
				 "INSERT INTO CollabInfo "//1
				   + "(displayName,linkName,projectID) "
				   + "VALUES( ? , ? , ? ) ";
		PreparedStatement stmt = conn.prepareStatement(query);
		stmt.setString(1,"Git");
		stmt.setString(2, gitURL);
		stmt.setInt(3, projectID);
		int success = stmt.executeUpdate();
		stmt.close();
		if(success>0) {
			stmt = conn.prepareStatement(query);
			stmt.setString(1,"Slack");
			stmt.setString(2, slackURL);
			stmt.setInt(3, projectID);
			success = stmt.executeUpdate();
			stmt.close();
			if(success>0) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * closes out the connection
	 * @throws SQLException
	 */
	public void closeConnection() throws SQLException {
		if(!conn.isClosed()) {
			conn.close();
		}
	}

	/**
	 * returns all artifacts associated with a project
	 * @param projectID
	 * @return ArrayList<Artifacts>
	 * @throws SQLException
	 */
	private ArrayList<Artifacts> getProjectArtifacts(int projectID) throws SQLException{
		ArrayList<Artifacts> artifacts = new ArrayList<Artifacts>();
		
		String query = 
				"SELECT name, displayName " +
				"FROM Artifact " +
				"WHERE projID = ?";
		
		PreparedStatement stmt = conn.prepareStatement(query);
		stmt.setInt(1, projectID);
		ResultSet result = stmt.executeQuery();
		
		while(result.next()) {
			artifacts.add(new Artifacts(result.getString(2),result.getString(1)));
		}
		
		result.close();
		stmt.close();
		return artifacts;
	}
	
	/**
	 * returns the thumbnail of the given project
	 * @param projectID
	 * @return Artifacts
	 * @throws SQLException
	 */
	private Artifacts getProjectThumbnail(int projectID) throws SQLException {
		
		Artifacts thumbnail = null;
		String query = 
				"SELECT displayName, name " +
				"FROM Artifact " +
				"WHERE projID = ? AND displayName='thumbnail'";
		
		PreparedStatement stmt = conn.prepareStatement(query);
		stmt.setInt(1, projectID);
		ResultSet result = stmt.executeQuery();
		if(result.next()) {
			thumbnail = new Artifacts(result.getString(1),result.getString(2));
		}else {
			thumbnail = new Artifacts(null,null);
		}
		
		result.close();
		stmt.close();
		return thumbnail;
	}

	/**
	 * gets all tags for a project
	 * @param projectID
	 * @return ArrayList<String>
	 * @throws SQLException
	 */
	private ArrayList<String> getProjectTags(int projectID) throws SQLException{
		ArrayList<String> tags = new ArrayList<String>();
		String query = 
				"SELECT tagName " +
				"FROM TagInformation " +
				"WHERE PID = ?";
		
		PreparedStatement stmt = conn.prepareStatement(query);
		stmt.setInt(1, projectID);
		ResultSet result = stmt.executeQuery();
		while(result.next()) {
			String name = result.getString(1);
			if(!result.wasNull()) {
				tags.add(name);
			}
		}
		
		result.close();
		stmt.close();
		return tags;
	}
	
	/**
	 * gets all collab listings for a project
	 * @param projectID
	 * @return ArrayList<Collaboration>
	 * @throws SQLException
	 */
	private ArrayList<Collaboration> getProjectCollab(int projectID) throws SQLException{
		ArrayList<Collaboration> collab = new ArrayList<Collaboration>();
		String query = 
				"SELECT displayName, linkName " +
				"FROM CollabInfo " +
				"WHERE projectID = ? ";
		
		PreparedStatement stmt = conn.prepareStatement(query);
		stmt.setInt(1, projectID);
		ResultSet result = stmt.executeQuery();
		while(result.next()) {
			String collabName = result.getString(1);
			String collabLink = result.getString(2);
			collab.add(new Collaboration(collabLink,collabName));
		}
		
		result.close();
		stmt.close();
		return collab;
	}
	
	/**
	 * gets all members for a given project
	 * @param projectID
	 * @return ArrayList<Member>
	 * @throws SQLException
	 */
	private ArrayList<Member> getProjectMembers(int projectID) throws SQLException {
		ArrayList<Member> members = new ArrayList<>();
		String query = 
				"SELECT PM.userID, X.firstName, X.lastName, PM.roleName " + 
				"FROM ProjectMember PM " + 
				"LEFT JOIN " + 
					"(SELECT U.firstName, U.lastName, U.email " + 
					"FROM User U) X " + 
					"ON PM.userID = X.email " + 
				"WHERE PM.projectID = ?" ;
		
		PreparedStatement stmt = conn.prepareStatement(query);
		stmt.setInt(1, projectID);
		ResultSet result = stmt.executeQuery();
		
		while (result.next()) {
			String email = result.getString(1);
			String fName = result.getString(2);
			String lName = result.getString(3);
			String role = result.getString(4);
			Member member = new Member(email,(fName +" "+ lName),role);
			members.add(member);
		}
		result.close();
		stmt.close();
		return members;
	}
	
}