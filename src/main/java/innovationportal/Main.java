package innovationportal;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.GeneralSecurityException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;


public class Main {
	
	static boolean close = false;
	static int MAX_PROJECTS = 12;
	public static void main(String[] args) throws IOException, GeneralSecurityException  {
		//site https://collaboration-testorg.slack.com/threads/

		//opens server socket on port 5069
		final int SOCKET_NUM = 5069;
		final int MAX_FEATURED_CACHE = 4;
		final int MAX_CACHE = MAX_PROJECTS - MAX_FEATURED_CACHE;
		Map<Integer,Integer> visitedCount = new HashMap<Integer,Integer>(); 
		ServerSocket serverSocket = new ServerSocket(SOCKET_NUM);
		
		//creates cache with a max of MAC_CACHE projects
		Cache<Integer,Projects> cache = new Cache<Integer, Projects>(MAX_CACHE);
		Cache<Integer,Projects> featuredCache = new Cache<Integer, Projects>(MAX_FEATURED_CACHE);
		int[] featuredIDs = {20,24,27,28};
		
		populateCache(cache,featuredCache,featuredIDs);
		
		//infinite loop so program never ends unless told to
		while(!close) {
			
			Socket currentClient = null;
			try {
				// accepts incoming socket connection
				currentClient = serverSocket.accept();
				currentClient.setKeepAlive(true);
				System.out.println("Client Connected: " + currentClient);
				
				//create streams for socket
				BufferedReader is = new BufferedReader(new InputStreamReader(currentClient.getInputStream()));
				DataOutputStream os = new DataOutputStream(currentClient.getOutputStream());
				
				//creates and starts thread
				Thread thread = new ThreadHandler(currentClient,is,os,cache,featuredCache,visitedCount);
				thread.start();
				if(close) {
					serverSocket.close();
				}
				
			}catch(Exception ex){
				currentClient.close();
				ex.printStackTrace();
			}
		}
		serverSocket.close();
	}
	private static void populateCache(Cache<Integer, Projects> cache, Cache<Integer, Projects> featuredCache,int[] featuredIDs) {
		Database data;
		try {
			data = new Database();
			data.populateFeaturedCache(featuredCache, featuredIDs);
			data.populateCache(cache,cache.maxSize()*2,featuredCache);
			data.closeConnection();
		} catch (SQLException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
