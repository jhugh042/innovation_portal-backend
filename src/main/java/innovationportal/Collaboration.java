package innovationportal;

public class Collaboration {
	private final String link;
	private final String name;
	
	Collaboration(String _link, String _name){
		link = _link;
		name = _name;
	}

	public String getName() {
		if(name != null) {
			return name;
		}
		return "null";
	}

	public String getLink() {
		if(link != null) {
			return link;
		}
		return "null";
	}
}
