package innovationportal;

/**
 * Artifacts class, stores artifact data for projects
 * @author JAM
 */
public class Artifacts{
	
	private final String artifactDisplayName;
	private final String artifactLocation;
	
	
	/**
	 * Artifact constructor
	 * 
	 * @param _artifactName
	 * @param _artifactDisplayName
	 * @param _artifactLocation
	 */
	Artifacts(String _artifactDisplayName, String _artifactLocation){
		artifactDisplayName = _artifactDisplayName;
		artifactLocation = _artifactLocation;
	}
	
	
	/**
	 * @return artifactDisplayname
	 */
	String getArtifactDisplayName() {
		if(artifactDisplayName !=null) {
			return artifactDisplayName;
		}
		return "null";
	}
	
	/**
	 * 
	 * @return artifactLocation
	 */
	String getArtifactLocation() {
		if(artifactLocation !=null) {
			return artifactLocation;
		}
		return "null";
	}
}