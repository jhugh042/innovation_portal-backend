package innovationportal;

import java.sql.Date;
import java.util.ArrayList;

/**
 * Store project data for cache and passing between ThreadHandler and Database
 * @author JAM
 */
public class Projects {

	private final int projectID;
	private final String displayName;
	private final String description;
	private final String thumbnailLoc;
	private ArrayList<Artifacts> artifacts;
	private ArrayList<Member> members;
	private String permissions;
	private ArrayList<Collaboration> collabs;
	private ArrayList<String> tags;
	
	private int views =0;
	private Date dateCreated;
	
	Projects(){
		projectID = -1;
		displayName = null;
		description = null;
		thumbnailLoc = null;
	}
	/**
	 * Constructor for displaycase project
	 * @param _projectID
	 * @param _displayName
	 * @param _description
	 * @param _thumbnailLoc
	 * @param featured
	 */
	Projects(int _projectID, String _displayName, String _description, String _thumbnailLoc){
		projectID = _projectID;
		displayName = _displayName;
		description = _description;
		thumbnailLoc = _thumbnailLoc;
	}

	/**
	 * constructor for My Projects page
	 * @param _projectID
	 * @param _displayName
	 * @param _description
	 * @param _thumbnailLoc
	 * @param _creator
	 * @param _moderator
	 * @param _admin
	 */
	Projects(int _projectID, String _displayName, String _description, 
					String _thumbnailLoc,String _permissions){
		projectID = _projectID;
		displayName = _displayName;
		description = _description;
		thumbnailLoc = _thumbnailLoc;
		setPermissions(_permissions);
	}
	
	/**
	 * Constructor for featured projects and single project view
	 * 
	 * @param _projectID
	 * @param _displayName
	 * @param _description
	 * @param _thumbnailLoc
	 * @param _artifacts
	 * @param _members
	 */
	Projects(int _projectID, String _displayName, String _description, String _thumbnailLoc,
			ArrayList<Artifacts> _artifacts, ArrayList<Collaboration> _collabs,ArrayList<String> _tags,ArrayList<Member> _members){
		projectID = _projectID;
		displayName = _displayName;
		description = _description;
		thumbnailLoc = _thumbnailLoc;
		artifacts = _artifacts;
		members = _members;
		collabs = _collabs;
		tags = _tags;
	}
	
	Projects(int _projectID, String _displayName,ArrayList<Member> _members){
		thumbnailLoc = null;
		description = null;
		projectID = _projectID;
		displayName = _displayName;
		members = _members;
	}
	
	/**
	 * Test constructor: TO BE DELETED
	 * @param _projectID
	 * @param _name
	 */
	Projects(int _projectID,String _name){
		projectID = _projectID;
		displayName = _name;
		description = null;
		thumbnailLoc = null;
	}

	/**
	 * adds artifact to arraylist
	 * @param _artifact
	 */
	public void addArtifact(Artifacts _artifact) {
		artifacts.add(_artifact);
	}
	
	/**
	 * adds members to arraylist
	 * @param _member
	 */
	public void addMembers(Member _member) {
		members.add(_member);
	}
	
	/**
	 * @return projectID
	 */
	public int getProjectID() {
		return projectID;
	}
	
	/**
	 * @return displayName
	 */
	public String getDisplayName() {
		return displayName;
	}
	
	/**
	 * 
	 * @return projectDescription
	 */
	public String getDescription() {
		return description;
	}
	
	/**
	 * 
	 * @return thumbnailLocation
	 */
	public String getThumbnailLocation() {
		if(thumbnailLoc != null) {
			return thumbnailLoc;
		}else {
			return "null";
		}	
	}
	
	/**
	 * @return members
	 */
	public ArrayList<Member> getMembers() {
		return members;
	}
	
	/** 
	 * get all artifacts for project
	 * @return artifacts for the project
	 */
	public ArrayList<Artifacts> getArtifacts() {
		return artifacts;
	}
	
	/**
	 * get permissions for project
	 * @return permission string
	 */
	public String getPermissions() {
		return permissions;
	}
	
	/**
	 * sets the permission of the project
	 * @param permissions
	 */
	public void setPermissions(String permissions) {
		this.permissions = permissions;
	}
	
	/**
	 * gets all collaboration info for project
	 * @return
	 */
	public ArrayList<Collaboration> getCollabs() {
		return collabs;
	}
	
	/**
	 * set collaboration info for project
	 * @param collabs
	 */
	public void setCollabs(ArrayList<Collaboration> collabs) {
		this.collabs = collabs;
	}
	
	/**
	 * get all tags for project
	 * @return array of tag strings
	 */
	public ArrayList<String> getTags() {
		return tags;
	}
	
	/**
	 * sets the tags for a project
	 * @param tags
	 */
	public void setTags(ArrayList<String> tags) {
		this.tags = tags;
	}
	
	/**
	 * returns the ammount of views for a project
	 * @return int views
	 */
	public int getViews() {
		return views;
	}
	
	/**
	 * sets the number of views for a project
	 * @param views
	 */
	public void setViews(int views) {
		this.views = views;
	}
	
	/**
	 * gets the date the project was created
	 * @return Date created
	 */
	public Date getDateCreated() {
		return dateCreated;
	}
	
	/**
	 * set the date created for a project
	 * @param dateCreated
	 */
	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}
}
