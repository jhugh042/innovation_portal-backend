package innovationportal;


import java.nio.file.LinkOption;
import java.util.ArrayList;
import java.util.Date;

import org.apache.commons.collections4.MapIterator;
import org.apache.commons.collections4.map.LRUMap;
 
/**
 * @Original_author by Crunchify.com
 * 
 *  @Changes by CS411 JAM
 */
 


public class Cache<K, T> {
    
	public static final long HOUR = 3600*1000;
	public static final long MINUTE = 60*1000;
	public static final long SECOND = 1*1000;
	private Date date;
    //the contents of the Cache
    private LRUMap cacheMap;
    private int maxSize;
 
    protected class CacheObject {
        public long lastAccessed = System.currentTimeMillis();
        public T value;
 
        protected CacheObject(T value) {
            this.value = value;
        }
    }
 
    /**
     * initializes cache with a max size
     * 
     * @param	maxItems
     * 			The maximum size of the cache
     */
    public Cache(int maxItems) {
    	this.updateTime();
        cacheMap = new LRUMap(maxItems);
        maxSize = maxItems;
    }
 
    /**
     * adds object to cache map uses key and value pair
     * 
     * @param 	key
     * 			key value of map
     * 
     * @param	value
     * 			value pair that goes with key
     */
    public void put(K key, T value) {
        synchronized (cacheMap) {
            cacheMap.put(key, new CacheObject(value));
        }
    }
    
    /**
     * Used if you dont want to pull object but want to keep it alive
     * @param key
     */
    public void touch(K key) {
    	
    	CacheObject cacheObject =(CacheObject) cacheMap.get(key);
    	cacheObject.lastAccessed = System.currentTimeMillis();
    	
    }
    
    /**
     * checks to see if a key value pair exists for key
     * 
     * @param key
     * @return true if exists false otherwise
     */
    public boolean exists(K key) {
    	if(cacheMap.get(key) != null) {
    		return true;
    	}else {
    		return false;
    	}
    }
 
    /**
     * returns value object associated with key value
     * @param 	key
     * 			key value of desired pair
     * 
     * @return	object of associated key
     */
    @SuppressWarnings("unchecked")
    public T get(K key) {
        synchronized (cacheMap) {
            CacheObject cacheObject = (CacheObject) cacheMap.get(key);
 
            if (cacheObject == null)
                return null;
            else {
            	cacheObject.lastAccessed = System.currentTimeMillis();
                return cacheObject.value;
            }
        }
    }
 
    /**
     * @return the number of items in cache
     */
    public int size() {
        synchronized (cacheMap) {
            return cacheMap.size();
        }
    }
    
    /**
     * 
     * @return the max size possible of cache
     */
    public int maxSize() {
    	return maxSize;
    }
 
    public boolean timeElapsed() {
    	if(date.before(new Date())) {
    		return true;
    	}
    	return false;
    }
    
    public void updateTime() {
    	date = new Date((new Date().getTime() + (5*MINUTE)));
    }
    
    /**
     * clears the entire cacheMap
     */
    @SuppressWarnings("unchecked")
    public void cleanup() {
    	cacheMap.clear();
    }
    
    /**
     * gets arraylist of all keys
     * 
     * @return ArrayList<k>
     */
    public ArrayList getKeys() {
    	ArrayList<K> keys = new ArrayList<>(cacheMap.size());
    	synchronized (cacheMap) {
        	MapIterator itr = cacheMap.mapIterator();
        	K key = null;
        	while (itr.hasNext()) {
        		key = (K) itr.next();
        		keys.add(key);
        	}
    	}
    	return keys;
    }
}