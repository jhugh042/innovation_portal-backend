package innovationportal;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.GeneralSecurityException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.AccessLevel;
import org.gitlab4j.api.models.Member;
import org.gitlab4j.api.models.Project;
import org.gitlab4j.api.models.User;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.DateTime;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.CalendarScopes;
import com.google.api.services.calendar.model.ConferenceData;
import com.google.api.services.calendar.model.ConferenceSolutionKey;
import com.google.api.services.calendar.model.CreateConferenceRequest;
import com.google.api.services.calendar.model.EntryPoint;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.EventAttendee;
import com.google.api.services.calendar.model.EventDateTime;
import com.google.api.services.calendar.model.EventReminder;

import allbegray.slack.SlackClientFactory;
import allbegray.slack.type.Channel;
import allbegray.slack.webapi.SlackWebApiClient;


public class API{

	static String slackToken= "xoxp-348801867158-348801867686-348960547319-c70ae858904b9012a563de81385d17ed";
	
	private final static char[] gitRootPass = "password".toCharArray();
	private final static String gitUser = "root";
	final static String gitLoc = "https://128.82.11.82:8929";
	//final static String gitLocLocalhost = "https://localhost:8929";
	private static GitLabApi gitLabApi;
	
    private static final String APPLICATION_NAME = "Innovation Portal";
    private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
    private static final String TOKENS_DIRECTORY_PATH = "tokens";
    private static final List<String> SCOPES = Collections.singletonList(CalendarScopes.CALENDAR);
    private static final String CREDENTIALS_FILE_PATH = "../credentials.json";
	
    //https://github.com/gmessner/gitlab4j-api#projectapi
    	//create new project
	
	
	static public String createSlackChannels(String channelName,ArrayList<String> members) {
		
		//https://collaboration-testorg.slack.com/
		
		//creates hook to slack page
		SlackWebApiClient webApiClient = SlackClientFactory.createWebApiClient(slackToken);
		//check if channel exists
		ArrayList<Channel> list = (ArrayList<Channel>) webApiClient.getChannelList();
		boolean channelExists=false;
		
		for(Channel channel : list) {
			if(channel.getName().equals(channelName)) {
				channelExists = true;
			}
		}
		if(!channelExists) {
			
			webApiClient.createChannel(channelName);
			//itterate channels to check if channel was created
			
			for(Channel channel : list) {
				if(channel.getName().equals(channelName)) {
					channel.setMembers(members);
				}
			}
		}
		return webApiClient.getWebApiUrl();
	}
	
	/**
	 * required for google authentication
	 * @param HTTP_TRANSPORT
	 * @return Credential
	 * @throws IOException
	 */
	private static Credential getCredentials(final NetHttpTransport HTTP_TRANSPORT) throws IOException {
        // Load client secrets.
		System.out.println(CREDENTIALS_FILE_PATH);
        InputStream in = API.class.getResourceAsStream(CREDENTIALS_FILE_PATH);
        GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));

        // Build flow and trigger user authorization request.
        GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
                HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPES)
                .setDataStoreFactory(new FileDataStoreFactory(new java.io.File(TOKENS_DIRECTORY_PATH)))
                .setAccessType("offline")
                .build();
        return new AuthorizationCodeInstalledApp(flow, new LocalServerReceiver()).authorize("user");

	}
	
	/**
	 * create hangout event with (string) title, (string) description, (string) start date, (string) end date, (bool) debug?
	 * @param 	_title
	 * 			title of the event
	 * @param 	_description
	 * 			description of the event
	 * @param 	_startDate
	 * 			start date of the event; YYYY-MM-DDT00:00:00-00:00
	 * @param 	_endDate
	 * 			end date of the event; YYYY-MM-DDT00:00:00-00:00
	 * @param 	DEBUG
	 * @return	true on success, false on failure
	 * @throws 	IOException
	 * @throws 	GeneralSecurityException
	 */
	public static boolean createHangout(String _title, String _description,String _startDate, String _endDate, boolean... DEBUG) throws IOException, GeneralSecurityException{
		//https://developers.google.com/calendar/quickstart/java
		//client ID: 1075346958036-pd35mbknnid46177r69kadk058lkrieo.apps.googleusercontent.com
		//client secret:413WMQNDwSjGGSTloKEpD5vP
		
		//creating event: https://developers.google.com/calendar/create-events

        final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
        Calendar service = new Calendar.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT))
                .setApplicationName(APPLICATION_NAME)
                .build();

        //creates the event with title and description
        Event event = new Event()
			    .setSummary(_title)
			    .setDescription(_description);
		
        //adds the start date/time to the event
		DateTime startDateTime = new DateTime(_startDate);
		EventDateTime start = new EventDateTime()
		    .setDateTime(startDateTime)
		    .setTimeZone("America/Los_Angeles");
		    //.setTimeZone("America/New_York US/Eastern");
		event.setStart(start);
		
		//adds the end date/time to the event
		DateTime endDateTime = new DateTime(_endDate);
		EventDateTime end = new EventDateTime()
			.setDateTime(endDateTime)
			.setTimeZone("America/Los_Angeles");
			//.setTimeZone("America/New_York US/Eastern");
		event.setEnd(end);
		
		//adds the list of attendees to the event
		EventAttendee[] attendees = new EventAttendee[] {
			    new EventAttendee().setEmail("j.m.hughes.jh@gmail.com")
			    //new EventAttendee().setEmail("jhugh042@odu.edu.com")
			};
		event.setAttendees(Arrays.asList(attendees));
		
		//sets the reminder type for the event, maybe add email reminder on event create?
		EventReminder[] reminderOverrides = new EventReminder[] {
			    new EventReminder().setMethod("email").setMinutes(24 * 60),
			    new EventReminder().setMethod("popup").setMinutes(30),
			};
		
		//removes the default reminders
		Event.Reminders reminders = new Event.Reminders()
			    .setUseDefault(false)
			    .setOverrides(Arrays.asList(reminderOverrides));
			event.setReminders(reminders);
		
		//primary keyword tells google to use current logged in user
		String calendarId = "primary";
		
		//sets entry point for hangout, needed to add hangout to event
		EntryPoint entry = new EntryPoint()
				.setEntryPointType("video")
				.setUri("http:");
		
		//tells the event the conference type, eventHangout for normal Hangout video call, links to ConferenceReqest
		ConferenceSolutionKey conferenceSolutionKey = new ConferenceSolutionKey()
			.setType("eventHangout");
		
		//not entirely sure the reason for this but im pretty sure its needed
		CreateConferenceRequest req =new CreateConferenceRequest()
			.setConferenceSolutionKey(conferenceSolutionKey)
			.setRequestId("hteryh65"); //random string, not sure if it needs to be different for each hangout
		
		//contains all the conference data thats added to the event
		ConferenceData conferenceData = new ConferenceData()
				.setEntryPoints(Arrays.asList(entry))
				.setCreateRequest(req);
		event.setConferenceData(conferenceData);
		
		//adds event to the calendar, ConferenceDataVersion is needed to add hangout, 1 for data 0 for no conference
		event = service.events().insert(calendarId, event).setConferenceDataVersion(1).execute();
		
		if(event.getHtmlLink()==null) {
			System.err.println("Error creating event");
			return false;
		}else if(event.getHangoutLink()==null) {
			System.err.println("Error creating Hangout for event");
			return false;
		}
		if((DEBUG != null) && DEBUG[0]) {
			removeEvent(event.getId());
		}
		return true;
	}
	
	/*
	 * removes an event, meant only for the test case of createHangout
	 */
	private static void removeEvent(String _eventID) throws GeneralSecurityException, IOException {
		// Initialize Calendar service with valid OAuth credentials
        final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
		Calendar service = new Calendar.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT))
                .setApplicationName(APPLICATION_NAME)
                .build();

		// Delete an event
		service.events().delete("primary", _eventID).execute();
	}
	

	/**
	 * creates git repository under root
	 * using root as the creator makes it so it cant be removed by users
	 * @param projectName
	 * @param projectDescription
	 * @param memberUsernames
	 * @param memberEmails
	 * @param memberFullNames
	 * @param creatorMember
	 * @throws GitLabApiException
	 * 
	 * TODO : Make sure no spaces in project name
	 */
	public static int createGit(String projectName, String projectDescription,String[] memberUsernames,
				String[] memberEmails, String[] memberFullNames, String creatorMember) throws GitLabApiException {
		
		//gitLabApi
		// Create a new project
		gitLabApi = GitLabApi.oauth2Login(gitLoc, gitUser, gitRootPass,true);
		if(gitLabApi == null) {
			System.out.println("Error logging into gitlab");
			return -1;
		}
		
		Project projectSpec = new Project()
		    .withName(projectName)
		    .withDescription(projectDescription)
		    .withIssuesEnabled(true)
		    .withMergeRequestsEnabled(true)
		    .withWikiEnabled(true)
		    .withSnippetsEnabled(true)
		    .withPublic(true);
		
		Project newProject = gitLabApi.getProjectApi().createProject(projectSpec);
		
		if(!gitLabApi.getProjectApi().getOptionalProject(newProject.getId()).isPresent()) {
			
			System.out.println("Error Creating project");
			return -1;
		}
		
		//checks if user exists, if not creates user
		int result = checkGitUsers(memberUsernames,memberEmails,memberFullNames, newProject.getId(),creatorMember)
					? newProject.getId() : -1;
				
				//*/
		return result;
	}
	
	/**
	 * 
	 * @param memberUsernames
	 * @param memberEmails
	 * @param memberFullNames
	 * @param projectID
	 * @param creatorMember
	 */
	@SuppressWarnings("deprecation")
	private static boolean checkGitUsers(String[] memberUsernames, String[] memberEmails, String[] memberFullNames, int projectID,String creatorMember) {
		//loop throught usernames
		for(int i=0;i<memberUsernames.length;i++) {
			try {
				//check if username already exists
				Optional<User> user = gitLabApi.getUserApi().getOptionalUser(memberUsernames[i]);
				if(!user.isPresent()) {
					//if not creates user
					if(!createGitUser(memberEmails[i],memberFullNames[i],memberUsernames[i])) {
						
						System.out.println("Error Creating user");
						return false;
					}
					user = gitLabApi.getUserApi().getOptionalUser(memberUsernames[i]);
				}
				Member member;
				if(memberUsernames.equals(creatorMember)) {
					member = gitLabApi.getProjectApi().addMember(projectID, user.get().getId(), AccessLevel.MASTER);
				}
				else {
					member = gitLabApi.getProjectApi().addMember(projectID, user.get().getId(), AccessLevel.DEVELOPER);
				}
				
				
				if(!gitLabApi.getProjectApi().getOptionalMember(projectID,member.getId()).isPresent()) {
					
					System.out.println("Error adding user to project");
					return false;
					
				}
				
			} catch (GitLabApiException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
	
	public static boolean removeGitUser(String username) {
		try {
			gitLabApi = GitLabApi.oauth2Login(gitLoc, gitUser, gitRootPass,true);
			Optional<User> user =gitLabApi.getUserApi().getOptionalUser(username); 
			if(user.isPresent()) {
				gitLabApi.getUserApi().deleteUser(gitLabApi.getUserApi().getUser(username));
			}
			
			return !user.isPresent();
		} catch (GitLabApiException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public static boolean removeGitProject(int projectID) {
		try {
			gitLabApi = GitLabApi.oauth2Login(gitLoc, gitUser, gitRootPass,true);
			Optional<Project> gitProject = gitLabApi.getProjectApi().getOptionalProject(projectID);
			if(gitProject.isPresent()) {
				gitLabApi.getProjectApi().deleteProject(projectID);
			}
			
			return !gitProject.isPresent();
		} catch (GitLabApiException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	
	/**
	 * creates user with no password, sends email to 
	 * listed email to create password
	 * @param 	email
	 * 			email of user
	 * @param 	fullName
	 * 			user full name
	 * @param 	username
	 * 			username for user
	 * @throws 	GitLabApiException
	 */
	private static boolean createGitUser(String email, String fullName, String username) throws GitLabApiException {
		User userConfig = new User()
			    .withEmail(email)
			    .withName(fullName)
			    .withUsername(username);
		
			String password = "password";
			boolean sendResetPasswordEmail = true;
			gitLabApi.getUserApi().createUser(userConfig, password, sendResetPasswordEmail);
			
			//checks if user was created correctly
			boolean result = gitLabApi
							.getUserApi()
							.getUser(username)
							.equals(null) 
							? false : true;	
			
			return result;
	}
}






