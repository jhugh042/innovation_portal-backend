package innovationportal;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.gitlab4j.api.GitLabApiException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import javax.mail.*; 
import javax.mail.internet.*; 
import javax.activation.*; 

//TODO Handle backend error checks
public class ThreadHandler extends Thread{

	private final BufferedReader is;
	private final DataOutputStream os;
	private final Socket socket;
	private Cache<Integer,Projects> cache;
	private Cache<Integer,Projects> featuredCache;
	private Database data = null;
	private Map<Integer,Integer> visitedCount;
	
	/**
	 * default constructor used for unit tests
	 * 
	 * @param _cache
	 */
	ThreadHandler(Cache<Integer,Projects> _cache){
		socket = null;
		os = null;
		is = null;
		cache = _cache;
		featuredCache = new Cache<Integer, Projects>(1);
		visitedCount = new HashMap<Integer, Integer>();
	}
	
	/**
	 * consctructor for normal use case
	 * @param _socket
	 * @param _is
	 * @param _os
	 * @param _cache
	 * @param _featuredCache
	 */
	ThreadHandler(Socket _socket, BufferedReader _is, DataOutputStream _os, 
			Cache<Integer,Projects> _cache, Cache<Integer,Projects> _featuredCache,
			Map<Integer,Integer> _visitedCount){
		
		socket = _socket;
		is = _is;
		os = _os;
		cache = _cache;
		featuredCache = _featuredCache;
		visitedCount = _visitedCount;
	}
	
	@Override
	public void run() {
		String data = null;
		
		try {
			data = is.readLine();
			returnDataFunction(handleResponse(data));
		} catch (IOException | ClassNotFoundException | InvalidKeySpecException | NoSuchAlgorithmException | SQLException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * handles what to do with the data thats been read
	 * @param data
	 * @return JSON String
	 * @throws IOException 
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 * @throws NoSuchAlgorithmException 
	 * @throws InvalidKeySpecException 
	 */
	@SuppressWarnings("unchecked")
	public String handleResponse(String data) throws ClassNotFoundException, SQLException, IOException, InvalidKeySpecException, NoSuchAlgorithmException {
		//convert string to json object
		JSONObject returnData = null;
		JSONParser parser = new JSONParser(); 
		JSONObject jsonData;
		try {
			jsonData = (JSONObject) parser.parse(data);
			String request = jsonData.get("request").toString();
			jsonData.remove("request");
			
			switch(request) {
			case "fdc-featured":
				returnData = homeTask();
				break;
				
			case "fdc-simple-search":
				jsonData.putIfAbsent("filter", "empty");
				//if filter was null
				if(jsonData.get("filter").equals("empty")) {
					returnData = failBackEndTask("filter element missing!","ThreadHandler - HandleResposnse", "NONE");
				}else {
					returnData = fillDisplayCaseTask(jsonData.get("filter").toString());
				}
				break;
				
			case "single-project":
				returnData = projectPopulateTask(jsonData.get("project_id").toString());
				break;
				
			case "joinRequest":
				returnData = joinRequestTask(jsonData);
				break;
				
			case "login":
				JSONObject creds = (JSONObject) parser.parse(jsonData.get("credentials").toString());
				returnData = loginTask(creds);
				break;
				
			case "register":
				returnData = registerUser(jsonData);
				break;
				
			case "create-project":
				returnData = this.createProject(jsonData);
				break;
				
			case "artifact_upload":
				returnData = artifactUploadTask(jsonData);
				break;
				
			case "artifact_delete":
				returnData = this.deleteArtifactTask(jsonData);
				break;
				
			case "quit":
				Main.close = true;
				break;
				
			case "fdc-my-projects":
				returnData = populateMyProjectsTask(jsonData);
				break;
				
			case "fdc-admin-view-all":
				returnData = this.adminAllProjects();
				break;
				
			case "fdc-newlyadded":
				returnData = this.projectsRecentlyAdded();
				break;
				
			case "fdc-frequently-viewed":
				returnData = this.projectsWithMostViews();
				break;
				
			case "fdc-requesting-moderator":
				returnData = this.projectsNeedingMod();
				break;
				
			case "project-add-user":
				returnData = this.addUserToProject(jsonData);
				break;
			default:
				returnData = defaultData(request);
				break;
				
			}
		} catch (ParseException e) {
			returnData = failBackEndTask("JSON Parse exception","ThreadHandler - HandleResposnse", e.toString());
			e.printStackTrace();
		}
		System.out.println("Sending back: " + returnData.toJSONString());
		return returnData.toJSONString();
	}
	
	/**
	 * populates the homepage using cache
	 * @return JSONObject
	 */
	@SuppressWarnings("unchecked")
	private JSONObject homeTask() {

		ArrayList<Integer> cacheKeys = cache.getKeys();
		ArrayList<Integer> featuredKeys = featuredCache.getKeys();
		
		int size = cache.size() + featuredCache.size();
		JSONObject object = new JSONObject();
		int i=0;
		//populates with featured cache first
		for(Integer key : featuredKeys) {
			Projects currentProject = featuredCache.get(key);
			object.put("project"+i, createProjectJSON(currentProject));
			i++;
		}
		//then handles regular cache
		for(Integer key : cacheKeys) {
			Projects currentProject = cache.get(key);
			object.put("project"+i, createProjectJSON(currentProject));
			i++;
		}
		object.put("count", size);
		object.put("response", "OK");
		return object;
	}
	
	/**
	 * Given a projectID, query the database for:
	 * 		projectMembers
	 * 		Project Artifacts
	 * 		Collab Info
	 * 		Tags
	 * 		Project info
	 * @param a String for the ID
	 * @return a JSONObject with the quried info
	 */
	@SuppressWarnings("unchecked")
	private JSONObject projectPopulateTask(String ID) {
		
		JSONObject message = new JSONObject();
		try {
			data = new Database();
			
			int pID = Integer.parseInt(ID);
			Projects project = data.getProject(pID);
			
			//gets ID,name,description,thumbnail
			message = this.createProjectJSON(project);
			
			JSONArray pArray = new JSONArray();
			JSONArray cArray = new JSONArray();
			JSONArray mArray = new JSONArray();
			JSONObject projectMembers = new JSONObject();
			
			//adds members to their respective role
			for(Member member : project.getMembers()) {
				JSONObject cMembers = new JSONObject();
				JSONObject mMembers = new JSONObject();
				JSONObject pMembers = new JSONObject();
				
				if(member.getMemberRole().contains("Creator")) {
					cMembers.put("email", member.getMemberName());
					cMembers.put("display-name", member.getMemberDisplayName());
					cArray.add(cMembers);
				}
				else if(member.getMemberRole().contains("Participant")) {
					pMembers.put("email", member.getMemberName());
					pMembers.put("display-name", member.getMemberDisplayName());
					pArray.add(pMembers);
				}
				else if(member.getMemberRole().contains("Moderator")) {
					mMembers.put("email", member.getMemberName());
					mMembers.put("display-name", member.getMemberDisplayName());
					mArray.add(pMembers);
				}
			}
			//puts those members in jsonObject
			projectMembers.put("creator", cArray);
			projectMembers.put("participants", pArray);
			projectMembers.put("moderators", mArray);
			
			//adds all the members to project_members key
			message.put("project_members", projectMembers);

			JSONArray aArray = new JSONArray();
			for(Artifacts artifact: project.getArtifacts()) {
				JSONObject artifacts = new JSONObject();
				artifacts.put("artifact-name", artifact.getArtifactDisplayName());
				artifacts.put("artifact-location", artifact.getArtifactLocation());
				aArray.add(artifacts);
			}
			message.put("project_artifacts", aArray);

			JSONArray tags = new JSONArray();
			for(String tag : project.getTags()) {
				tags.add(tag);
			}
			message.put("project_tags", tags);
			
			
			JSONArray collabArray = new JSONArray();
			for(Collaboration collab: project.getCollabs()) {
				JSONObject collabs = new JSONObject();
				collabs.put("collab-name", collab.getName());
				collabs.put("collab-uri", collab.getLink());
				collabArray.add(collabs);
			}
			message.put("project_collabs", collabArray);
			message.put("response", "OK");
			
			if(visitedCount.get(pID) != null) {
				visitedCount.put(pID, visitedCount.get(pID)+1);
			}
			else {
				visitedCount.put(pID, 1);
			}
			
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
			return failBackEndTask("Error creating database object","ThreadHandler - projectPopulateTask()",e.toString());
		}
		
		return message;
	}

	/**
	 * returns a failure message with location and a stacktrace
	 * 
	 * @param errorMessage
	 * @param location
	 * @param stack trace
	 * @return JSONObject
	 */
	@SuppressWarnings("unchecked")
	private JSONObject failBackEndTask(String errorMessage, String location, String e) {
		
		JSONObject message = new JSONObject();
		JSONObject details = new JSONObject();
		
		details.put("message", errorMessage);
		details.put("number", location);
		details.put("stack-trace", e);
		
		message.put("response", "backend-exception");
		message.put("exception-details", details);
		
		return message;
	}
	
	/**
	 * Given a projectID, query the database for:
	 * 		Creator Email
	 * 
	 * uses email to send message
	 *  Runtime.getRuntime().exec("echo "This is the body" | mail -s "This is the subject" user@gmail.com")
	 *  
	 * @return JSONObject
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 * @throws IOException 
	 */
	@SuppressWarnings("unchecked")
	private JSONObject joinRequestTask(JSONObject jsonData){
		
		JSONObject returnData = new JSONObject();
		String requestor = jsonData.get("requestor").toString();
		int projectID = Integer.parseInt(jsonData.get("project").toString());

		try {
			data = new Database();
			String creator = data.getProjectCreatorEmail(projectID);
			
	        String from = "innovationportal@odu.edu";
	        String host = "localhost"; // or IP address 
	  
	        // Get the session object 
	        // Get system properties 
	        Properties properties = System.getProperties();  
	  
	        // Setup mail server 
	        properties.setProperty("mail.smtp.host", host);  
	  
	        // Get the default Session object 
	        Session session = Session.getDefaultInstance(properties);  
	  
	        // compose the message 
	  
	        // javax.mail.internet.MimeMessage class  
	        // is mostly used for abstraction. 
	        MimeMessage message = new MimeMessage(session);  
	  
	        // header field of the header. 
	        message.setFrom(new InternetAddress(from));  
	        message.addRecipient(Message.RecipientType.TO,  
	                                new InternetAddress(creator)); 
	        message.setSubject("Someone wants to join your project!"); 
	        message.setText("Someone would like to join your project on innovationportal, would you like to allow them?"
	        		+ "https://innovationportal.cs.odu.edu/adduser.php?email="+requestor+"&project_id="+projectID); 
	  
	        // Send message 
	        Transport.send(message); 
	        
		} catch (ClassNotFoundException | SQLException  | MessagingException e) {
			e.printStackTrace();
			return failBackEndTask("Error sending request","ThreadHandler - joinRequestTask", e.toString());
		}
		
		returnData.put("response", "OK");
		return returnData;
	}
	
	@SuppressWarnings("unchecked")
	private JSONObject addUserToProject(JSONObject jsonData){
		JSONObject returnData = new JSONObject();
		String userEmail = jsonData.get("email").toString();
		int projectID = Integer.parseInt(jsonData.get("project_id").toString());
		try {
			Database data = new Database();
			if(!data.appendProjectMember(userEmail, projectID)) {
				return failBackEndTask("Error updating table","ThreadHandler - addUserToProject", "NONE");
			}
			
	        String from = "innovationportal@odu.edu";
	        String host = "localhost"; // or IP address 
			
	        // Get the session object 
	        // Get system properties 
	        Properties properties = System.getProperties();  
	  
	        // Setup mail server 
	        properties.setProperty("mail.smtp.host", host);  
	  
	        // Get the default Session object 
	        Session session = Session.getDefaultInstance(properties);  
	  
	        // compose the message 
	  
	        // javax.mail.internet.MimeMessage class  
	        // is mostly used for abstraction. 
	        MimeMessage message = new MimeMessage(session);  
	  
	        // header field of the header. 
	        message.setFrom(new InternetAddress(from));  
	        message.addRecipient(Message.RecipientType.TO,  
	                                new InternetAddress(userEmail)); 
	        message.setSubject("You've been Accepted"); 
	        message.setText("The project creator accepted you into the project!"); 
	  
	        // Send message 
	        Transport.send(message); 
			
		} catch (ClassNotFoundException | SQLException | MessagingException e) {
			e.printStackTrace();
			return failBackEndTask("Error connecting to the DB","ThreadHandler - addUserToProject", e.toString());
		}
		
		returnData.put("response", "OK");
		return returnData;
	}
	
	/**
	 * Given a projectID, update DB with artifact
	 * @param a JSONObject for the data
	 * @return a JSON string with the queried info
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 */
	@SuppressWarnings("unchecked")
	private JSONObject artifactUploadTask(JSONObject jsonData) {

		JSONObject returnData = new JSONObject();
		
		try {
			//create DB connection
			data = new Database();
			
			//gets needed information
			int projectID = Integer.parseInt(jsonData.get("projectID").toString());
			String location = jsonData.get("artifact-location").toString();
			String type = jsonData.get("artifact-type").toString();
			
			//changes response depending if artifact was uploaded
			if (data.uploadArtifact(projectID, location, type)){
				returnData.put("response", "OK");
			}
			else{
				return failBackEndTask("failed to upload artifact","ThreadHandler - artifactUpload", "failed to insert data to DB");
			}
			
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
			return failBackEndTask("Error uploading artifact","ThreadHandler - artifactUploadTest", e.toString());
		}

		//change this return when done
		return returnData;
	}
	
	/**
	 *  given a project id and user email query the database for:
	 *  	project name,
	 *  	project description,
	 *  	and image url.
	 * @return JSONObject
	 */
	@SuppressWarnings("unchecked")
	private JSONObject populateMyProjectsTask(JSONObject jsonData) {
		
		//gets the username and list of projects
		JSONObject returnData = new JSONObject();
		String username = jsonData.get("username").toString();
		ArrayList<Integer> projectsList = new ArrayList<>();
		if(jsonData.get("projects") != null) {
			JSONArray jsonArray = (JSONArray) jsonData.get("projects");
			for(int i = 0; i < jsonArray.size();i++) {
				projectsList.add(Integer.parseInt(jsonArray.get(i).toString()));
			}
		}
		try {
			//connects the the DB
			data = new Database();
			
			JSONObject creator = new JSONObject();
			JSONObject moderator = new JSONObject();
			JSONObject participant = new JSONObject();
			JSONObject projects = new JSONObject();
			
			int i = 0;
			//loop through each project
			for(Integer projectID : projectsList){
				
				//call DB's getProject for each projectID in array
				Projects project = data.getProject(projectID);
				
				//loop through members
				for(Member member : project.getMembers()) {
					//if the member equals the username
					if(member.getMemberName().equals(username)) {
						
						String role = member.getMemberRole();
						//add the project to respective role
						if(role.contains("Creator")) {
							creator.put("project"+i, createProjectJSON(project));
						}
						else if(role.contains("Mentor")) {
							moderator.put("project"+i, createProjectJSON(project));
						}
						else if(role.contains("Participant")) {
							participant.put("project"+i, createProjectJSON(project));
						}
					}
				}
				i++;
			}
			projects.put("creator", creator);
			projects.put("moderator", moderator);
			projects.put("participant", participant);
			returnData.put("projects", projects);
			returnData.put("response", "OK");
			
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
			return failBackEndTask("Failed connection to database","ThreadHandler - populateMyProjectsTask", e.toString());
		}
		
		return returnData;
	}
	
	/**
	 * attempts to login with the given credentials
	 * @param jsonData
	 * @return JSONObject
	 */
	@SuppressWarnings("unchecked")
	private JSONObject loginTask(JSONObject jsonData) {
		
		JSONObject returnData = new JSONObject();
		JSONObject credentials = (JSONObject) jsonData;
		JSONObject userDef = new JSONObject();
		String username = credentials.get("username").toString();
		String password = credentials.get("password").toString();
		
		try {
			data = new Database();
		
			Member loginMember = data.loginFunction(username, password);
			if(loginMember != null)
			{
				userDef.put("useremail", loginMember.getMemberEmail());
				userDef.put("userdisplayname", loginMember.getMemberDisplayName());
				userDef.put("username", loginMember.getMemberName());
		
				//Let's get all the permissions for this user
				JSONObject permissions = new JSONObject();
				
				permissions.put("specialUserFlag", loginMember.getPermissionType());
		
				if (loginMember.getCreatorProjects() != null)
					permissions.put("creator", loginMember.getCreatorProjects());
		
				if (loginMember.getParticipantProjects() != null)
					permissions.put("participant", loginMember.getParticipantProjects());
		
				if (loginMember.getModeratorProjects() != null)
					permissions.put("moderator", loginMember.getModeratorProjects());
				
				userDef.put("permissions", permissions);
	
			}
		} catch (ClassNotFoundException | SQLException | InvalidKeySpecException |NoSuchAlgorithmException e) {
			e.printStackTrace();
			String stack = e.toString();
			if(stack.contains(password)) {
				stack = stack.replaceAll(password, "*****");
			}
			return failBackEndTask("Error logging in","ThreadHandler - loginTask",stack);
		}
		returnData.put("response", "OK");
		returnData.put("userdef", userDef);
		
		return returnData;
	}
	
	/**
	 * task that fills the display case of UI
	 * @param filter
	 * @return JSONObject
	 */
	@SuppressWarnings("unchecked")
	private JSONObject fillDisplayCaseTask (String filter)
	{
		try {
			data = new Database();
			
			String[] filterArray = null;
			ArrayList<Projects> projectsArray = new ArrayList<Projects>();
			if (filter.contains(","))
			{
				filterArray = filter.split(",");		
			}
			else if (filter.contains(" "))
			{
				filterArray = filter.split(" ");
			}else {
				filterArray = new String[] {filter};
			}
			for (int x = 0; x < filterArray.length; x++)
			{
				ArrayList<Projects> temp = data.searchFunction(filterArray[x]);
				for(Projects loop : temp) {
					if(!projectsArray.contains(loop)) {
						projectsArray.add(loop);
					}
				}
			}
			JSONObject object = new JSONObject();
			int i=0;
			
			for(Projects projects : projectsArray) 
			{
				JSONObject projectObject = this.createProjectJSON(projects);
				object.put("project"+i, projectObject);
				i++;
			}
			object.put("response","OK");
			object.put("count", projectsArray.size());
			
			return object;
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
			return failBackEndTask("Error loading displaycase information","ThreadHandler - fillDisplayCase",e.toString());
		} 
	}
	
	/**
	 * creates a project and updates the database
	 * @param jsonData
	 * @return JSONObject
	 */
	@SuppressWarnings("unchecked")
	private JSONObject createProject(JSONObject jsonData) {
		JSONObject returnData = new JSONObject();
		
		String creator = jsonData.get("creator").toString();
		String projectName = jsonData.get("project_name").toString();
		String projectDescription = jsonData.get("project_description").toString();
		
		
		try {
			data = new Database();
			
			int id = data.createProject(creator, projectName, projectDescription);
			if(id != -1) {
				//data for creating git repo
				Member temp = data.getUserInfo(creator);
				String[] usernames = new String[] {temp.getMemberDisplayName().replaceAll(" ", "_")};
				String[] emails = new String[] {creator};
				String[] fullName = new String[] {temp.getMemberName()};
				API.createGit(projectName, projectDescription, usernames, emails, fullName, creator);
				
				//create gitURL
				String realPName = projectName.replaceAll(" ", "-").toLowerCase();
				String gitURL = API.gitLoc +"/root/" + realPName;
				
				//create slack url
				String slackURL = API.createSlackChannels(realPName, new ArrayList<String>(Arrays.asList(creator)));
				
				data.appendCollabInfo(id, gitURL, slackURL);
				
				returnData.put("response", "OK");
				returnData.put("project_id", id);
			}else {
				return this.failBackEndTask("Failed to create project", "ThreadHandler - createProject", "");
			}
		} catch (ClassNotFoundException | SQLException | GitLabApiException e) {
			e.printStackTrace();
			return this.failBackEndTask("Failed to create project", "ThreadHandler - createProject", e.toString());
		}
		return returnData;
	}
	
	/**
	 * gets select data from all projects for Admin view
	 * @return JSONObject
	 */
	@SuppressWarnings("unchecked")
	private JSONObject adminAllProjects() {
		JSONObject returnData = new JSONObject();
		JSONObject projectsObj = new JSONObject();
		
		try {
			data = new Database();
			ArrayList<Projects> projects = data.getAllProjects();
			int i=0;
			for(Projects currentProject : projects) {
				
				JSONObject currentObj = new JSONObject();
				currentObj.put("project_id", currentProject.getProjectID());
				currentObj.put("project_name", currentProject.getDisplayName());
				for(Member member : currentProject.getMembers()) {
					if(member.getMemberRole().contains("Mentor")) {
						currentObj.put("mentor_email", member.getMemberName());
						currentObj.put("mentor_displayName", member.getMemberDisplayName());
					}
				}
				projectsObj.put("project"+i, currentObj);
				i++;
			}
			returnData.put("projects", projectsObj);
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
			return this.failBackEndTask("Failed to get projects", "ThreadHandler - adminAllProjects", e.toString());
		}
		returnData.put("response", "OK");
		return returnData;
	}
	
	/**
	 * registers a user in the database
	 * @param jsonData
	 * @return JSONObject
	 */
	@SuppressWarnings("unchecked")
	private JSONObject registerUser(JSONObject jsonData) {
		
		JSONObject returnData = new JSONObject();
		String username = jsonData.get("username").toString();
		String password = jsonData.get("password").toString();
		String displayName = jsonData.get("userdisplayname").toString();
		String fName = jsonData.get("first-name").toString();
		String lName = jsonData.get("last-name").toString();
		try {
			data = new Database();
			if(data.registerUser(username, password, fName, lName, displayName)) {
				JSONObject userdef = new JSONObject();
				JSONObject permissions = new JSONObject();
				
				permissions.put("specialUserFlag", "user");
				permissions.put("creator",new JSONArray());
				permissions.put("moderator",new JSONArray());
				permissions.put("participant",new JSONArray());
				
				userdef.put("userdisplayname", displayName);
				userdef.put("useremail", username);
				userdef.put("username", fName +" "+lName);
				userdef.put("permissions", permissions);
				
				returnData.put("userdef", userdef);
				returnData.put("response", "OK");
			}
			else {
				JSONObject userdef = new JSONObject();
				returnData.put("userdef", userdef);
				returnData.put("response", "OK");
			}
		} catch (ClassNotFoundException | SQLException | NoSuchAlgorithmException | InvalidKeySpecException e) {
			e.printStackTrace();
			return this.failBackEndTask("failed to register user", "ThreadHandler - registerUser", e.toString());
		}

		return returnData;
	}
	
	/**
	 * returns all projects that do not have moderators
	 * @return JSONObject
	 */
	@SuppressWarnings("unchecked")
	private JSONObject projectsNeedingMod() {
		JSONObject returnData = new JSONObject();
		
		try {
			data = new Database();
			ArrayList<Projects> projects = data.getProjectsNeedingMod();
			
			for(int i =0; i<projects.size(); i++) {
				
				JSONObject projObj = new JSONObject();
				Projects current = projects.get(i);
				
				projObj.put("project_id", current.getProjectID());
				projObj.put("project_name", current.getDisplayName());
				projObj.put("project_description", current.getDescription());
				projObj.put("image_url", current.getThumbnailLocation());
				
				returnData.put("project"+i, projObj);
				
			}
			
			returnData.put("count", projects.size());
			returnData.put("response", "OK");
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
			return this.failBackEndTask("failed to get projects", "ThreadHandler - projectsNeedingMod", e.toString());
		}
		
		return returnData;
	}
	
	/**
	 * gets the projects with the highest view count
	 * @return JSONObject
	 */
	@SuppressWarnings("unchecked")
	private JSONObject projectsWithMostViews() {
		JSONObject returnData = new JSONObject();
		
		try {
			data = new Database();
			ArrayList<Projects> projects = data.getProjectsByViews();
			
			for(int i =0; i<projects.size(); i++) {
				
				JSONObject projObj = new JSONObject();
				Projects current = projects.get(i);
				
				projObj.put("project_id", current.getProjectID());
				projObj.put("project_name", current.getDisplayName());
				projObj.put("project_description", current.getDescription());
				projObj.put("image_url", current.getThumbnailLocation());
				projObj.put("viewcount", current.getViews());
				
				returnData.put("project"+i, projObj);
				
			}
			
			returnData.put("count", projects.size());
			returnData.put("response", "OK");
			
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
			return this.failBackEndTask("failed to get projects", "ThreadHandler - projectsWithMostViews", e.toString());
		}
		
		return returnData;
	}
	
	@SuppressWarnings("unchecked")
	private JSONObject projectsRecentlyAdded() {
		JSONObject returnData = new JSONObject();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-dd-mm"); 
		try {
			data = new Database();
			ArrayList<Projects> projects = data.getProjectsByDate();
			
			for(int i =0; i<projects.size(); i++) {
				
				JSONObject projObj = new JSONObject();
				Projects current = projects.get(i);
				
				projObj.put("project_id", current.getProjectID());
				projObj.put("project_name", current.getDisplayName());
				projObj.put("project_description", current.getDescription());
				projObj.put("image_url", current.getThumbnailLocation());
				projObj.put("date", dateFormat.format(current.getDateCreated()));
				
				returnData.put("project"+i, projObj);
				
			}
			
			returnData.put("count", projects.size());
			returnData.put("response", "OK");
			
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
			return this.failBackEndTask("failed to get projects", "ThreadHandler - projectsRecentlyAdded", e.toString());
		}
		
		return returnData;
	}
	
	@SuppressWarnings("unchecked")
	private JSONObject deleteArtifactTask(JSONObject jsonData) {
		
		//setup variables
		JSONObject returnData = new JSONObject();
		int projectID = Integer.parseInt(jsonData.get("projectID").toString());
		ArrayList<String> artifactType = new ArrayList<String>();
		
		//gets all artifacts from array
		if(jsonData.get("artifacts") != null) {
			JSONArray jsonArray = (JSONArray) jsonData.get("artifacts");
			for(int i = 0; i < jsonArray.size();i++) {
				artifactType.add(jsonArray.get(i).toString());
			}
		}
		//if arraylist is empty there was a problem
		if(artifactType.isEmpty()) {
			return this.failBackEndTask("artifactType is empty", "ThreadHandler - deleteArtifactTask", "NONE");
		}else {
			try {
				//connect to DB and remove the artifacts
				Database data = new Database();
				for(String value : artifactType) {
					int success = data.removeProjectArtifact(projectID, value);
					if(success < 0) {
						return this.failBackEndTask("Failed to remove artifact from DB", "ThreadHandler - deleteArtifactTask", "NONE");
					}
				}
			} catch (ClassNotFoundException | SQLException e) {
				e.printStackTrace();
				return this.failBackEndTask("Database failure", "ThreadHandler - deleteArtifactTask", e.toString());
			}
		}
		returnData.put("response", "OK");
		return returnData;
	}
	
	/**
	 * writes to the outputstream,
	 * closes the database connection,
	 * closes streams and socket
	 * @param jsonString
	 */
	private void returnDataFunction(String jsonString) {
		try {
			os.writeBytes(jsonString);
			this.is.close();
			this.os.close();
			if(cache.timeElapsed()) {
				this.timeUpdate();
			}
			if(data != null) {
				data.closeConnection();
			}
			this.socket.close();
			return;
		} catch (IOException e) {
			System.out.println("Error writing to socket: "+e.toString() );
			e.printStackTrace();
		} catch (SQLException e) {
			System.out.println("Error closing database: "+e.toString() );
			e.printStackTrace();
		}
	}
	
	/**
	 * runs this code every hour,
	 * updates the cache and submits project views
	 */
	private void timeUpdate() {
		try {
			cache.updateTime();
			if(data == null) {
				data = new Database();
			}
			data.populateCache(cache, cache.size()/2, featuredCache);
			data.updateViews(visitedCount);
			System.out.println("Cache has been updated");
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
			System.out.println("Failed to update cache");
		}
	}
	
	/**
	 * writes default data for missing code/tasks
	 * @param task
	 * @return JSONObject
	 */
	private JSONObject defaultData(String task) {
		JSONObject returnData = new JSONObject();
		System.out.println("Missing code for task: "+task);
		returnData.put("response", "Missing Code for task");
		return returnData;
	}
	
	@SuppressWarnings("unchecked")
	private JSONObject createProjectJSON(Projects currentProject) {
		JSONObject project = new JSONObject();
		
		project.put("project_id", currentProject.getProjectID());
		project.put("project_name", currentProject.getDisplayName());
		project.put("project_description", currentProject.getDescription());
		project.put("image_url", currentProject.getThumbnailLocation());
		
		return project;
	}
}
