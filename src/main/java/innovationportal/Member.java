package innovationportal;

import java.util.ArrayList;

/*
 * members class that contains member data for project
 */
public class Member{
	private String memberName = null;
	private String memberDisplayName = null;
	private String memberRole= null;
	
	//variables only used for login
	private String email = null;
	private String permissionType = null;
	private ArrayList<Integer> creator = null;
	private ArrayList<Integer> moderator = null;
	private ArrayList<Integer> participant = null;
	
	/**
	 * project constructor
	 * @param _memberName
	 * @param _memberDisplayName
	 * @param _memberRole
	 */
	Member(String _memberName, String _memberDisplayName, String _memberRole){
		memberName = _memberName;
		memberDisplayName = _memberDisplayName;
		setMemberRole(_memberRole);
	}
	
	/**
	 * login constructor
	 * @param _memberName
	 * @param _memberDisplayName
	 * @param _email 
	 * @param _permissionType overal site permissions
	 * @param _creator arraylist of project ids they created
	 * @param _moderator arraylist of project ids they moderate
	 * @param _participant arraylist of project ids they participate in
	 */
	Member(String _memberName, String _memberDisplayName, String _email, String _permissionType,
			ArrayList<Integer> _creator,ArrayList<Integer> _moderator,ArrayList<Integer> _participant){
		
		memberName = _memberName;
		memberDisplayName = _memberDisplayName;
		setEmail(_email);
		setPermissionType(_permissionType);
		setCreator(_creator);
		setModerator(_moderator);
		setParticipant(_participant);
	}
	
	/**
	 * @return member name
	 */
	String getMemberName() {
		return memberName;
	}
	
	/**
	 * @return member display name
	 */
	String getMemberDisplayName() {
		return memberDisplayName;
	}

	/**
	 * returns member email
	 * @return String email
	 */
	public String getMemberEmail() {
		return email;
	}

	/**
	 * sets member email
	 * @param email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * returns member overall permissions
	 * @return String permissions
	 */
	public String getPermissionType() {
		return permissionType;
	}

	/**
	 * sets member overal permission
	 * @param permissionType
	 */
	public void setPermissionType(String permissionType) {
		this.permissionType = permissionType;
	}

	/**
	 * returns projects where they are creators
	 * @return ArrayList of project ID's
	 */
	public ArrayList<Integer> getCreatorProjects() {
		return creator;
	}

	/**
	 * sets creator projects
	 * @param creator
	 */
	public void setCreator(ArrayList<Integer> creator) {
		this.creator = creator;
	}

	/**
	 * returns projects where they are moderator
	 * @return ArrayList of project ID's
	 */
	public ArrayList<Integer> getModeratorProjects() {
		return moderator;
	}

	/**
	 * sets moderator projects
	 * @param moderator
	 */
	public void setModerator(ArrayList<Integer> moderator) {
		this.moderator = moderator;
	}

	/**
	 * returns projects where they are participants
	 * @return ArrayList of project ID's
	 */
	public ArrayList<Integer> getParticipantProjects() {
		return participant;
	}

	/**
	 * sets participant projects
	 * @param participant
	 */
	public void setParticipant(ArrayList<Integer> participant) {
		this.participant = participant;
	}

	/**
	 * get member role for a project
	 * @return memberRole
	 */
	public String getMemberRole() {
		return memberRole;
	}

	/**
	 * set member role in a project
	 * @param memberRole
	 */
	public void setMemberRole(String memberRole) {
		this.memberRole = memberRole;
	}
}