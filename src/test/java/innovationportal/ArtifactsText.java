package innovationportal;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ArtifactsText {

	Artifacts artifact = null;
	Artifacts artifact2 = null;
	@Before
	public void setUp() throws Exception {
		artifact = new Artifacts("name","/path/");
		artifact2 = new Artifacts(null,null);
	}

	@After
	public void tearDown() throws Exception {
		artifact = null;
		artifact2 = null;
	}

	@Test
	public void getDisplayNameTest() {
		assertEquals("name",artifact.getArtifactDisplayName());
		assertEquals("null",artifact2.getArtifactDisplayName());
	}
	
	@Test
	public void getLocationTest() {
		assertEquals("/path/",artifact.getArtifactLocation());
		assertEquals("null",artifact2.getArtifactLocation());
	}

}
