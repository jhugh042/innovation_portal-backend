package innovationportal;

import static org.junit.Assert.*;

import java.io.IOException;
import java.security.GeneralSecurityException;

import org.gitlab4j.api.GitLabApiException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class APITest {

	@Before
	public void setUp() throws Exception {
		
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void createHangoutTest() {
		try {
			if(!API.createHangout("API Test_title","Desc","2018-10-28T09:00:00-07:00","2018-10-28T17:00:00-07:00",true)) {
				fail("Failed to create event");
			}
		} catch (IOException | GeneralSecurityException e) {
			fail("Failed to create event");
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("static-access")
	@Test
	public void createGitTest() {
		String[] arrayUser = {"TestUser1"};
		String[] arrayEmail = {"jhugh042@odu.edu"};
		String[] arrayName = {"Jacob Hughes"};
		try {
			
			int result = -1;
			result = API.createGit("Test_Project", "Test_Description", arrayUser, 
					arrayEmail, arrayName, "Jacob_Hughes");
			System.out.println("Project ID (-1 is error): "+ result);
			
			assertTrue(result != -1);
			
			API.removeGitUser(arrayUser[0]);
			API.removeGitProject(result);
			
		} catch (GitLabApiException e) {
			e.printStackTrace();
			fail("Error creating Gitlab project");
		}
	}
}