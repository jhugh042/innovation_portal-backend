package innovationportal;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class CollaborationTest {

	Collaboration collab;
	Collaboration collab2;
	@Before
	public void setUp() throws Exception {
		collab = new Collaboration("https://link","name");
		collab2 = new Collaboration(null,null);
	}

	@After
	public void tearDown() throws Exception {
		collab = null;
		collab2 = null;
	}

	@Test
	public void getLinkTest() {
		assertEquals("https://link",collab.getLink());
		assertEquals("null",collab2.getLink());
	}
	
	@Test
	public void getNameTest() {
		assertEquals("name",collab.getName());
		assertEquals("null",collab2.getName());
	}

}
