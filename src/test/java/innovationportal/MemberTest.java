package innovationportal;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class MemberTest {
	
	Member member = null;
	@Before
	public void setUp() throws Exception {
		ArrayList<Integer> array = new ArrayList();
		member = new Member("name","displayName","email","permission",
				array,array,array);
	}

	@After
	public void tearDown() throws Exception {
		member = null;
	}

	@Test
	public void getMemberNameTest() {
		assertEquals("name",member.getMemberName());
	}
	
	@Test
	public void getMemberDisplayNameTest() {
		assertEquals("displayName",member.getMemberDisplayName());
	}
	
	@Test
	public void memberEmailTest() {
		member.setEmail("test");
		assertEquals("test",member.getMemberEmail());
	}
	
	@Test
	public void memberPermissionTest() {
		member.setPermissionType("test");
		assertEquals("test",member.getPermissionType());
	}
	
	@Test
	public void memberCreatorTest() {
		ArrayList<Integer> array = new ArrayList();
		array.add(1);
		array.add(10);
		member.setCreator(array);
		assertEquals(array,member.getCreatorProjects());
	}
	
	@Test
	public void memberModTest() {
		ArrayList<Integer> array = new ArrayList();
		array.add(1);
		array.add(10);
		member.setModerator(array);
		assertEquals(array,member.getModeratorProjects());
	}
	
	@Test
	public void memberParticipantTest() {
		ArrayList<Integer> array = new ArrayList();
		array.add(1);
		array.add(10);
		member.setParticipant(array);
		assertEquals(array,member.getParticipantProjects());
	}
	
	@Test
	public void memberRoleTest() {
		member.setMemberRole("test");
		assertEquals("test",member.getMemberRole());
	}
}
