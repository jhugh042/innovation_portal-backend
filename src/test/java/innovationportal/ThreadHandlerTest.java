package innovationportal;

import static org.junit.Assert.*;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.SQLException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

@SuppressWarnings("unchecked")
public class ThreadHandlerTest {

	ThreadHandler test;
	Cache<Integer,Projects> cache;
	Database data;
	JSONParser parcer;
	JSONObject obj;
	//int[] featuredIDs = {20,24,27,28};
	Cache<Integer,Projects> featuredCache = new Cache(1);
	
	@Before
	public void setUp() throws Exception {
		cache = new Cache<Integer, Projects>(2);
		test = new ThreadHandler(cache);
		data = new Database();
		parcer = new JSONParser();
		data.populateCache(cache, cache.maxSize(),featuredCache);
		//featuredCache = new Cache<Integer, Projects>(4);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test 
	public void loginTest() {
		test = new ThreadHandler(cache);
		JSONObject request = new JSONObject();
		JSONObject creds = new JSONObject();
		creds.put("username", "admin.user@innovationportal.odu.edu");
		creds.put("password", "password");
		request.put("request", "login");
		request.put("credentials", creds.toString());
		
		try {
			String data = test.handleResponse(request.toString());
			assertTrue(data.contains("username"));
		} catch (ClassNotFoundException | InvalidKeySpecException | NoSuchAlgorithmException | SQLException
				| IOException e) {
			fail(e.toString());
			e.printStackTrace();
		}
	}
	
	@Test
	public void homeTest() throws ClassNotFoundException, InvalidKeySpecException, NoSuchAlgorithmException, SQLException, IOException {
		/*cache = new Cache<Integer, Projects>(2);
		test = new ThreadHandler(cache);
		try {
			data = new Database();
			data.populateCache(cache,cache.maxSize());
			data = null;
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		System.out.println(test.homeTask());*/
		assertTrue(true);
		
		//create JSON String then pass it in
		String data = "{" + "\"request\":\"fdc-featured\"" + "}\n";
		
		String response = test.handleResponse(data);
		
		assertTrue(cache.size()>0);
		assertTrue(response.contains("project_id"));
		assertTrue(response.contains("project_name"));
		assertTrue(response.contains("project_description"));
		assertTrue(response.contains("image_url"));
		//CHECK for all ids
		//String data = "home";
		
		
	}
	
	@Test
	public void temporaryCheck() {
		obj = new JSONObject();
		obj.put("request", "single-project");
		obj.put("project_id", 59);
		String response;
		try {
			response = test.handleResponse(obj.toString());
			System.out.println("HELLO:" + response);
			assertTrue(true);
		} catch (ClassNotFoundException | InvalidKeySpecException | NoSuchAlgorithmException | SQLException
				| IOException e) {
			fail("Exception thrown");
			e.printStackTrace();
		}
	}
	
	@Test
	public void singlePageTest() throws ClassNotFoundException, InvalidKeySpecException, NoSuchAlgorithmException, SQLException, IOException {
		assertTrue(true);
		String data = "{" + "\"request\":\"single-project\","
				+ "\"project_id\":11" + "}\n";
		
		String response = test.handleResponse(data);
		
		assertTrue(cache.size()>0);
		assertTrue(response.contains("project_id"));
		assertTrue(response.contains("project_name"));
		assertTrue(response.contains("project_description"));
		assertTrue(response.contains("image_url"));		
		
	}
	
	@Test
	public void fillDisplayCaseTest() throws ClassNotFoundException, InvalidKeySpecException, NoSuchAlgorithmException, SQLException, IOException {
		obj = new JSONObject();
		obj.put("request", "fdc-simple-search");
		obj.put("filter", null);
		String response = test.handleResponse(obj.toString());
		//System.out.println("NULL: "+ response);
		assertTrue(response.contains("filter element missing!"));
		obj.put("filter", "red");
		response = test.handleResponse(obj.toString());
		//System.out.println("NOT NULL: "+response);
		assertTrue(response.contains("OK"));
	}
	
	@Test
	public void temp() throws ClassNotFoundException, InvalidKeySpecException, NoSuchAlgorithmException, SQLException, IOException {
		obj = new JSONObject();
		obj.put("request", "fdc-my-projects");
		obj.put("username", "creator.user@innovationportal.odu.edu");
		JSONArray testArray = new JSONArray();
		testArray.add(11);
		testArray.add(13);
		testArray.add(12);
		obj.put("projects", testArray);
		System.out.println("X Array: "+testArray.toString());
		System.out.println("X Sending: "+obj.toString());
		String response = test.handleResponse(obj.toString());
		System.out.println("X "+response);
		assert(true);
	}

}
