package innovationportal;

import static org.junit.Assert.*;

import java.sql.Date;
import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ProjectsTest {

	Projects project = null;
	Projects project2 = null;
	@Before
	public void setUp() throws Exception {
		project = new Projects();
		ArrayList<Artifacts> _a = new ArrayList<Artifacts>(); 
		ArrayList<Collaboration> _c = new ArrayList<Collaboration>(); 
		ArrayList<String> _t = new ArrayList<String>();
		ArrayList<Member> _m = new ArrayList<Member>(); 
		project2 = new Projects(1,"displayN","desc","/path/",_a,_c,
				_t,_m);
	}

	@After
	public void tearDown() throws Exception {
		project = null;
		project2 = null;
	}

	@Test
	public void dateCreatedTest() {
        Date now = new Date(new java.util.Date().getTime());
		project.setDateCreated(now);
		assertEquals(now,project.getDateCreated());
	}
	
	@Test
	public void viewsTest() {
		project.setViews(654);
		assertEquals(654,project.getViews());
	}
	
	@Test
	public void tagsTest() {
		ArrayList<String> tags = new ArrayList<String>();
		tags.add("t1");
		tags.add("t2");
		project.setTags(tags);
		assertEquals(tags,project.getTags());
	}
	
	@Test
	public void collabsTest() {
		ArrayList<Collaboration> collabs = new ArrayList<Collaboration>();
		collabs.add(new Collaboration(null,null));
		collabs.add(new Collaboration("val","collab"));
		project.setCollabs(collabs);
		assertEquals(collabs,project.getCollabs());
	}

	@Test
	public void permissionsTest() {
		project.setPermissions("test");
		assertEquals("test",project.getPermissions());
	}
	
	@Test
	public void getArtifactsTest() {
		ArrayList<Artifacts> _a = new ArrayList<Artifacts>();
		Artifacts a = new Artifacts("","");
		_a.add(a);
		project2.addArtifact(a);
		assertEquals(_a,project2.getArtifacts());
	}
	
	@Test
	public void getMembersTest() {
		ArrayList<Member> _m = new ArrayList<Member>();
		Member m = new Member("","","");
		_m.add(m);
		project2.addMembers(m);
		assertEquals(_m,project2.getMembers());
	}
	
	@Test
	public void getThumbnailLocTest() {
		assertEquals("/path/", project2.getThumbnailLocation());
	}
	
	@Test
	public void getDescriptionTest() {
		assertEquals("desc",project2.getDescription());
	}
	
	@Test
	public void getDisplayNameTest() {
		assertEquals("displayN",project2.getDisplayName());
	}
	
	@Test
	public void getIDTest() {
		assertEquals(1,project2.getProjectID());
	}
}
