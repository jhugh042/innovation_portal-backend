package innovationportal;

import static org.junit.Assert.*;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import org.hsqldb.Server;

public class DatabaseTest  {

	Database data;
	static Server server;
	static Connection conn;
	private Cache<Integer,Projects> cache;
	Cache<Integer,Projects> featuredCache = new Cache(1);
	
    private static Connection getConnection() throws SQLException {
    	return DriverManager.getConnection("jdbc:hsqldb:mem:test", "sa", "");
    }

	
	@BeforeClass
	public static void init() throws Exception{
		//sets the DB driver
		Class.forName("org.hsqldb.jdbc.JDBCDriver");
		
		//https://stackoverflow.com/questions/1497855/how-to-run-a-hsqldb-server-in-memory-only-mode
		server = new Server();
		server.setDatabaseName(0, "test");
		server.setDatabasePath(0, "mem:test");
		server.setSilent(true);
		
		server.start();
		
		conn = getConnection();
		
		//creates tables to be used
		createTables(conn);
		
		//populate tables
		addUsers(conn);
		addProjects(conn);
		addTags(conn);
		addPMembers(conn);
		addCollab(conn);
		addArtifact(conn);		
		
	}
	
	@AfterClass
	public static void shutdown() throws Exception{
		
		server.shutdown();
	}
	
	@Before
	public void setUp() throws Exception {
		data = new Database(conn);
	}

	@After
	public void tearDown() throws Exception {
		
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void populateFeaturedCacheTest() throws SQLException{
		int array[] = {1};
		data.populateFeaturedCache(featuredCache,array);
		assertEquals(1,featuredCache.size());
		ArrayList<Integer> keys = featuredCache.getKeys();
		for(Integer id: keys) {
			assertTrue( id > (-1) );
		}
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void populateCacheTest() throws SQLException{
		cache = new Cache<Integer, Projects>(9);
		data.populateCache(cache, cache.maxSize(),featuredCache);
		assertEquals(9,cache.size());
		ArrayList<Integer> keys = cache.getKeys();
		for(Integer id: keys) {
			assertTrue( id > (-1) );
		}
	}
	
	@Test
	public void getProjectTest() throws SQLException{
		Projects proj = data.getProject(1);
		assertEquals(proj.getProjectID(),1);
		assertEquals(proj.getDisplayName(),"Test Project 2 Yellow");
		assertEquals(proj.getArtifacts().size(),1);
		assertEquals(proj.getThumbnailLocation(),"loc/to/file");
		assertEquals(proj.getTags().size(),1);
		assertEquals(proj.getCollabs().size(),3);
		assertEquals(proj.getMembers().size(),3);
	}
	
	@Test
	public void getUserPermissionsTest() throws SQLException{
		String response= data.getUserPermissions("big@red.com");
		assertEquals("User", response);
		
		response= data.getUserPermissions("Mack@black.com");
		assertEquals("Admin", response);
		
		response= data.getUserPermissions("jerry@green.com");
		assertEquals("Moderator", response);
	}
	
	@Test
	public void getUserRoleTest() throws SQLException{
		String role = data.getUserRole(1, "gary@green.com");
		assertEquals("Creator", role);
	}
	
	@Test
	public void getProjectCreatorTest() throws SQLException{
		String creator = data.getProjectCreator(0);
		assertEquals("Big Red",creator);
	}
	
	@Test
	public void getProjectCreatorEmailTest() throws SQLException{
		String response = "";
		response = data.getProjectCreatorEmail(0);
		assertEquals("big@red.com",response);
	}
	
	@Test
	public void searchFunctionTest() throws SQLException{
		
		ArrayList<Projects> temp = data.searchFunction("Red");
		assertEquals(1,temp.size());
		temp = data.searchFunction("Project");
		assertEquals(9,temp.size());
	}
	
	@Test
	public void uploadArtifactTest() throws SQLException{
		boolean result = data.uploadArtifact(1, "/path/", "thumbnail");
		assertTrue(result);
	}
	
	@Test
	public void registerAndLogginUserTest() {
		try {
			boolean result = data.registerUser("test@odu.edu", "password", "Test", "User", "tUser");
			
			assertTrue(result);
			
			Member user = data.loginFunction("test@odu.edu", "password");
			assertNotNull(user);
			
			
		} catch (InvalidKeySpecException | NoSuchAlgorithmException | SQLException e) {
			fail(e.toString());
			e.printStackTrace();
		}
	}
	
	@Test
	public void updateViewsTest() throws SQLException{
		assertTrue(true);
	}
	
	@Test
	public void createProjectTest() throws SQLException{
		assertTrue(true);
	}
	
	@Test
	public void getAllProjectsTest() throws SQLException{
		assertTrue(true);
	}
	
	@Test
	public void ugetProjectsNeedingModTest() throws SQLException{
		assertTrue(true);
	}
	
	@Test
	public void getProjectsByViewsTest() throws SQLException{
		assertTrue(true);
	}
	
	@Test
	public void getProjectsByDateTest() throws SQLException{
		assertTrue(true);
	}
	
	@Test
	public void removeProjectArtifactTest() throws SQLException{
		assertTrue(true);
	}
	
	@Test
	public void getUserInfoTest() throws SQLException{
		Member temp = data.getUserInfo("big@red.com");
		assertEquals("Big Red",temp.getMemberName());
	}
	
	@Test
	public void appendProjectMemberTest() throws SQLException{
		boolean result = data.appendProjectMember("big@red.com",1);
		assertTrue(result);
	}
	
	@Test
	public void appendCollabInfoTest() throws SQLException{
		boolean result = data.appendCollabInfo(1, "git", "slack");
		assertTrue(result);
	}	
	
 	public static void createTables(Connection conn) throws SQLException {
		
		Statement stmt = conn.createStatement();
		stmt.executeUpdate("CREATE TABLE  User ("//User
				   + "email VARCHAR(50) NOT NULL,"
				   + "firstName VARCHAR(25) NOT NULL,"
				   + "lastName VARCHAR(25) NOT NULL,"
				   + "displayName VARCHAR(25) NULL,"
				   + "phone VARCHAR(15) NULL,"
				   + "address VARCHAR(30) NULL,"
				   + "photo VARCHAR(30) NULL,"
				   + "passwordSalt VARCHAR(128) NOT NULL,"
				   + "passwordHash VARCHAR(128) NOT NULL,"
				   + "permission VARCHAR(30) NOT NULL,"
				   + "PRIMARY KEY (email)); "
				   + "CREATE TABLE  Project ("//Project
				   + "projectID INTEGER IDENTITY,"
				   + "displayName VARCHAR(25) NOT NULL,"
				   + "description VARCHAR(256) NOT NULL,"
				   + "lastUpdated DATE DEFAULT NOW," //DATE NOT NULL,
				   + "pageHitCount INTEGER NOT NULL,"
				   + "featured TINYINT NOT NULL,"
				   + "locked TINYINT NOT NULL,"
				   + "permission VARCHAR(30) NULL); "
				   + "CREATE TABLE  Artifact ("//Artifact
				   + "artifactID INTEGER IDENTITY,"
				   + "name VARCHAR(50) NOT NULL,"
				   + "displayName VARCHAR(50) NOT NULL,"
				   + "projID INTEGER NOT NULL); "
				   + "CREATE TABLE  CollabInfo ("//CollabInfo
				   + "collabID INTEGER IDENTITY,"
				   + "linkName VARCHAR(50) NOT NULL,"
				   + "displayName VARCHAR(50) NOT NULL,"
				   + "projectID INTEGER NOT NULL); "
				   + "CREATE TABLE  ProjectMember ("//ProjectMember
				   + "memberID INTEGER IDENTITY,"
				   + "userID VARCHAR(50) NOT NULL,"
				   + "roleName VARCHAR(15) NOT NULL,"
				   + "projectID INTEGER NOT NULL); "
				   + "CREATE TABLE  TagInformation ("//TagInformation
				   + "tagID INTEGER IDENTITY,"
				   + "tagName VARCHAR(50) NOT NULL,"
				   + "PID INTEGER NOT NULL); "
				   + "");
	}
	
	public static void addUsers(Connection conn) throws SQLException {
		Statement stmt = conn.createStatement();
		stmt.executeUpdate(
					 "INSERT INTO User "//1
				   + "(email,firstName,lastName,displayName,passwordSalt,passwordHash,permission)"
				   + "VALUES('big@red.com','Big','Red','BigRed','00000','000000','User');"
				   + "INSERT INTO User "//2
				   + "(email,firstName,lastName,displayName,passwordSalt,passwordHash,permission)"
				   + "VALUES('bob@gray.com','Bob','Gray',null,'00000','000000','User');"
				   + "INSERT INTO User "//3
				   + "(email,firstName,lastName,displayName,passwordSalt,passwordHash,permission)"
				   + "VALUES('gary@green.com','Gary','Green',null,'00000','000000','User');"
				   + "INSERT INTO User "//4
				   + "(email,firstName,lastName,displayName,passwordSalt,passwordHash,permission)"
				   + "VALUES('jack@black.com','Jack','Black',null,'00000','000000','Admin');"
				   + "INSERT INTO User "//5
				   + "(email,firstName,lastName,displayName,passwordSalt,passwordHash,permission)"
				   + "VALUES('jane@red.com','Jane','Red',null,'00000','000000','User');"
				   + "INSERT INTO User "//6
				   + "(email,firstName,lastName,displayName,passwordSalt,passwordHash,permission)"
				   + "VALUES('Mack@black.com','Mack','Black',null,'00000','000000','Admin');"
				   + "INSERT INTO User "//7
				   + "(email,firstName,lastName,displayName,passwordSalt,passwordHash,permission)"
				   + "VALUES('harry@gray.com','Harry','Gray',null,'00000','000000','User');"
				   + "INSERT INTO User "//8
				   + "(email,firstName,lastName,displayName,passwordSalt,passwordHash,permission)"
				   + "VALUES('sally@gray.com','Sally','Gray',null,'00000','000000','User');"
				   + "INSERT INTO User "//9
				   + "(email,firstName,lastName,displayName,passwordSalt,passwordHash,permission)"
				   + "VALUES('gordon@green.com','Gordon','Green',null,'00000','000000','Moderator');"
				   + "INSERT INTO User "//10
				   + "(email,firstName,lastName,displayName,passwordSalt,passwordHash,permission)"
				   + "VALUES('jerry@green.com','Jerry','Green',null,'00000','000000','Moderator');"
				   + "");
	}
	
	public static void addProjects(Connection conn) throws SQLException{
		Statement stmt = conn.createStatement();
		stmt.executeUpdate(
					 "INSERT INTO Project "//1
				   + "(displayName,description,pageHitCount,featured,locked)"
				   + "VALUES('Test Project 1 Red','This is test project 1',1,0,1); "
				   + "INSERT INTO Project "//2
				   + "(displayName,description,pageHitCount,featured,locked)"
				   + "VALUES('Test Project 2 Yellow','This is test project 2',1,0,1); "
				   + "INSERT INTO Project "//3
				   + "(displayName,description,pageHitCount,featured,locked)"
				   + "VALUES('Test Project 3 Black','This is test project 3',1,0,1); "
				   + "INSERT INTO Project "//4
				   + "(displayName,description,pageHitCount,featured,locked)"
				   + "VALUES('Test Project 4 Gray','This is test project 4',1,0,1); "
				   + "INSERT INTO Project "//5
				   + "(displayName,description,pageHitCount,featured,locked)"
				   + "VALUES('Test Project 5 Green','This is test project 5',1,0,1); "
				   + "INSERT INTO Project "//6
				   + "(displayName,description,pageHitCount,featured,locked)"
				   + "VALUES('Test Project 6 Orange','This is test project 6',1,0,1); "
				   + "INSERT INTO Project "//7
				   + "(displayName,description,pageHitCount,featured,locked)"
				   + "VALUES('Test Project 7 Blue','This is test project 7',1,0,1); "
				   + "INSERT INTO Project "//8
				   + "(displayName,description,pageHitCount,featured,locked)"
				   + "VALUES('Test Project 8 Violet','This is test project 8',1,0,1); "
				   + "INSERT INTO Project "//9
				   + "(displayName,description,pageHitCount,featured,locked)"
				   + "VALUES('Test Project 9 Sapphire','This is test project 9',1,0,1); "
				   + "");
	}
	
	public static void addTags(Connection conn) throws SQLException {
		Statement stmt = conn.createStatement();
		stmt.executeUpdate(
					 "INSERT INTO TagInformation "//1
				   + "(tagName,PID)"
				   + "VALUES('cs411',0); "
				   + "INSERT INTO TagInformation "//2
				   + "(tagName,PID)"
				   + "VALUES('computerscience',0); "
				   + "INSERT INTO TagInformation "//3
				   + "(tagName,PID)"
				   + "VALUES('cs411',1); "
				   + "INSERT INTO TagInformation "//4
				   + "(tagName,PID)"
				   + "VALUES('cs410',2); "
				   + "INSERT INTO TagInformation "//5
				   + "(tagName,PID)"
				   + "VALUES('cs410',3); "
				   + "");
	}

	public static void addPMembers(Connection conn) throws SQLException {
		Statement stmt = conn.createStatement();
		stmt.executeUpdate(
					 "INSERT INTO ProjectMember "//PID=0
				   + "(projectID,userID,roleName)"
				   + "VALUES(0,'big@red.com','Creator');"
				   + "INSERT INTO ProjectMember "//PID=0
				   + "(projectID,userID,roleName)"
				   + "VALUES(0,'bob@gray.com','Participant');"
				   + "INSERT INTO ProjectMember "//PID=0
				   + "(projectID,userID,roleName)"
				   + "VALUES(0,'jerry@green.com','Moderator');"
				   + "INSERT INTO ProjectMember "//PID=1
				   + "(projectID,userID,roleName)"
				   + "VALUES(1,'gary@green.com','Creator');"
				   + "INSERT INTO ProjectMember "//PID=1
				   + "(projectID,userID,roleName)"
				   + "VALUES(1,'jane@red.com','Participant');"
				   + "INSERT INTO ProjectMember "//PID=1
				   + "(projectID,userID,roleName)"
				   + "VALUES(1,'gordon@green.com','Moderator');"
				   + "");
	}
	
	public static void addCollab(Connection conn) throws SQLException {
		Statement stmt = conn.createStatement();
		stmt.executeUpdate(
					 "INSERT INTO CollabInfo "//0
				   + "(projectID,linkName,displayName)"
				   + "VALUES(0,'LINK','Git');"
				   + "INSERT INTO CollabInfo "//0
				   + "(projectID,linkName,displayName)"
				   + "VALUES(0,'LINK','Google');"
				   + "INSERT INTO CollabInfo "//0
				   + "(projectID,linkName,displayName)"
				   + "VALUES(0,'LINK','Slack');"
				   + "INSERT INTO CollabInfo "//1
				   + "(projectID,linkName,displayName)"
				   + "VALUES(1,'LINK','Git');"
				   + "INSERT INTO CollabInfo "//1
				   + "(projectID,linkName,displayName)"
				   + "VALUES(1,'LINK','Google');"
				   + "INSERT INTO CollabInfo "//1
				   + "(projectID,linkName,displayName)"
				   + "VALUES(1,'LINK','Slack');"
				   + "");
	}
	
	public static void addArtifact(Connection conn) throws SQLException {
		Statement stmt = conn.createStatement();
		stmt.executeUpdate(
					 "INSERT INTO Artifact "//0
				   + "(projID,name,displayName)"
				   + "VALUES(0,'loc/to/file','thumbnail');"
				   + "INSERT INTO Artifact "//1
				   + "(projID,name,displayName)"
				   + "VALUES(1,'loc/to/file','thumbnail');"
				   + "INSERT INTO Artifact "//2
				   + "(projID,name,displayName)"
				   + "VALUES(2,'loc/to/file','thumbnail');"
				   + "INSERT INTO Artifact "//3
				   + "(projID,name,displayName)"
				   + "VALUES(3,'loc/to/file','thumbnail');"
				   + "INSERT INTO Artifact "//4
				   + "(projID,name,displayName)"
				   + "VALUES(4,'loc/to/file','thumbnail');"
				   + "INSERT INTO Artifact "//5
				   + "(projID,name,displayName)"
				   + "VALUES(5,'loc/to/file','thumbnailx');"
				   + "");
	}
}
