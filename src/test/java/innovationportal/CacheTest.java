package innovationportal;

import static org.junit.Assert.*;

import java.util.Iterator;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class CacheTest {

	Cache<Integer, Projects> cache;
	Projects project;
	Projects project2;
	Iterator keys;
	@Before
	public void setUp() throws Exception {
		cache = new Cache<Integer, Projects>(5);
		project = new Projects(1,"Test1","desc1","thumbLoc1");
		cache.put(project.getProjectID(), project);
		project = new Projects(3,"Test3","desc3","thumbLoc3");
		cache.put(project.getProjectID(), project);
		project = new Projects(5,"Test5","desc5","thumbLoc5");
		cache.put(project.getProjectID(), project);
		project = new Projects(2,"Test2","desc2","thumbLoc2");
		cache.put(project.getProjectID(), project);
		project = new Projects(4,"Test4","desc4","thumbLoc4");
		cache.put(project.getProjectID(), project);
	}

	@After
	public void tearDown() throws Exception {
		cache.cleanup();
		cache = null;
		project = null;
		keys = null;
	}

	@Test
	public void thrreadSafeTest() {
		//System.out.println("\n\n==========Cache Test: Add Remove Objects==========");
		final Cache<Integer, Projects> cache2 = new Cache<Integer, Projects>(5);
		Thread t1 = new Thread(new Runnable() {
			public void run() {
				project = new Projects(1,"Test1","desc1","thumbLoc1");
				cache2.put(project.getProjectID(), project);
				project = new Projects(3,"Test3","desc3","thumbLoc3");
				cache2.put(project.getProjectID(), project);
				project = new Projects(5,"Test5","desc5","thumbLoc5");
				cache2.put(project.getProjectID(), project);
				return;
			}
		});
		Thread t2 = new Thread(new Runnable() {
			public void run() {
				project2 = new Projects(2,"Test2","desc2","thumbLoc2");
				cache2.put(project2.getProjectID(), project2);
				project2 = new Projects(4,"Test4","desc4","thumbLoc4");
				cache2.put(project2.getProjectID(), project2);
				project2 = new Projects(7,"Test7","desc7","thumbLoc7");
				cache2.put(project2.getProjectID(), project2);
				return;
			}
		});
		t1.start();
		t2.start();
		try {
			t2.join();
			t1.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		assertEquals(cache.size(),5);
		
	}
	
	@Test
	public void cacheCleanupTest() {
		
		cache.cleanup();
		assertEquals(cache.size(),0);
	}
	
	@Test
	public void cacheReplacementTest() {
		
		project = new Projects(6,"Test6","desc6","thumbLoc6");
		cache.put(project.getProjectID(), project);
		
		assertEquals(cache.size(),5);
		assertEquals(project,cache.get(6));
	}
	
	@Test
	public void cacheGetKeysTest() {
		assertEquals(cache.getKeys().size(),5);
	}
	
	@Test
	public void cacheTouchTest() {
		cache.touch(1);
		project = new Projects(10,"Test10","desc10","thumbLoc10");
		cache.put(project.getProjectID(), project);
		project = new Projects(11,"Test10","desc10","thumbLoc10");
		cache.put(project.getProjectID(), project);
		project = new Projects(12,"Test10","desc10","thumbLoc10");
		cache.put(project.getProjectID(), project);
		project = new Projects(13,"Test10","desc10","thumbLoc10");
		cache.put(project.getProjectID(), project);
		
		assertTrue(cache.exists(1));
	}
}
