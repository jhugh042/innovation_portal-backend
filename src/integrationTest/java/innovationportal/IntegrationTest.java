package innovationportal;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


public class IntegrationTest {

	static Socket client;
	static DataOutputStream os = null;
	static BufferedReader br = null;
	@BeforeClass
	static public void start() {
		try {
			client = new Socket("localhost",5069);
			os = new DataOutputStream(client.getOutputStream());
			br = new BufferedReader(new InputStreamReader(client.getInputStream()));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	
	@AfterClass
	static public void end() throws Exception {
		
		String endRun ="{"
				+ "\"request\":\"quit\""
				+ "}\n";
		os.writeBytes(endRun);
		os.flush();
		
		os.close();
		br.close();
		client.close();
	}

	@Test
	public void connectionTest() {
		if(client.isConnected()) {
			assertTrue(true);
			System.out.println("Connection is open");
		}else {
			fail("Did not connect!");
		}
	}
	
	@Test
	public void loginTest() {
		//fail("Not implemented");
	}

}
